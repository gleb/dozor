      subroutine iscancl(switch,value,failure)
      implicit none
      character*(*)switch
      integer value
      logical failure
c
      integer iargc
      integer narg
      character*256 str
      character*256 opt
      integer oplen
      integer lnblnk
c
      integer argc
c
      narg = iargc()
c      opt(1:1)='-'
      oplen = lnblnk(switch)!+1
      opt(1:oplen)=switch(1:oplen)
      do argc = 1,narg
         call getarg(argc,str)
         if( str(1:lnblnk(str)) .eq. opt(1:oplen)) then
            call getarg(argc+1,str)
            read(str,*,err=2,end=2)value
            failure=.false.
            return
         endif
      enddo
      failure = .true.
      return
 2    write(0,*)'Warning: failed to read integer number after ',
     +          opt(1:oplen),' command line option'  
      failure = .true.
      return
      end
      
      subroutine i3scancl(switch,value,failure)
      implicit none
      character*(*)switch
      integer value(3)
      logical failure
c
      integer iargc
      integer narg
      character*256 str
      character*256 opt
      integer oplen
      integer lnblnk,i
c
      integer argc
c
      narg = iargc()
c      opt(1:1)='-'
      oplen = lnblnk(switch)!+1
      opt(1:oplen)=switch(1:oplen)
      do argc = 1,narg
         call getarg(argc,str)
         if( str(1:lnblnk(str)) .eq. opt(1:oplen)) then
	  do i=1,3
            call getarg(argc+i,str)
            read(str,*,err=2,end=2)value(i)
	   enddo 
            failure=.false.
            return
         endif
      enddo
      failure = .true.
      return
 2    write(0,*)'Warning: failed to read integer number after ',
     +          opt(1:oplen),' command line option'  
      failure = .true.
      return
      end      
      
      
      

      subroutine r8scancl(switch,value,failure)
      implicit none
      character*(*)switch
      real*8 value
      logical failure
c
      integer iargc
      integer narg
      character*256 str
      character*256 opt
      integer oplen
      integer lnblnk
c
      integer argc
c
      narg = iargc()
c      opt(1:1)='-'
      oplen = lnblnk(switch)!+1
      opt(1:oplen)=switch(1:oplen)!-1)
      do argc = 1,narg
         call getarg(argc,str)
         if( str(1:lnblnk(str)) .eq. opt(1:oplen)) then
            call getarg(argc+1,str)
            read(str,*,err=2,end=2)value
            failure=.false.
            return
         endif
      enddo
      failure = .true.
      return
 2    write(0,*)'Warning: failed to read floating point number after ',
     +          opt(1:oplen),' command line option'  
      failure = .true.
      return
      end

      subroutine r4scancl(switch,value,failure)
      implicit none
      character*(*)switch
      real*4 value
      logical failure
c
      integer iargc
      integer narg
      character*256 str
      character*256 opt
      integer oplen
      integer lnblnk
c
      integer argc
c
      narg = iargc()
c      opt(1:1)='-'
      oplen = lnblnk(switch)!+1
      opt(1:oplen)=switch(1:oplen)!-1)
      do argc = 1,narg
         call getarg(argc,str)
         if( str(1:lnblnk(str)) .eq. opt(1:oplen)) then
            call getarg(argc+1,str)
            read(str,*,err=2,end=2)value
            failure=.false.
            return
         endif
      enddo
      failure = .true.
      return
 2    write(0,*)'Warning: failed to read floating point number after ',
     +          opt(1:oplen),' command line option'  
      failure = .true.
      return
      end

      subroutine rv4scancl(switch,value,length,failure)
      implicit none
      character*(*)switch
      real*4 value(*)
      integer length
      logical failure
c
      integer iargc
      integer narg
      character*256 str
      character*256 opt
      integer oplen
      integer i
c
      integer lnblnk
      integer argc
c
      narg = iargc()
c      opt(1:1)='-'
      oplen = lnblnk(switch)!+1
      opt(1:oplen)=switch(1:oplen)!-1)
      do argc = 1,narg
         call getarg(argc,str)
         if( str(1:lnblnk(str)) .eq. opt(1:oplen)) then
            do i=1,length
               call getarg(argc+i,str)
               read(str,*,err=2,end=2)value(i)
            enddo
            failure=.false.
            return
         endif
      enddo
      failure = .true.
      return
 2    write(0,'(a,i4,a)')'Warning: failed to read ',length
     + ,' floating point number(s) after '//
     +opt(1:oplen)//' command line option'
      failure = .true.
      return
      end



      subroutine cscancl(switch,value,failure)
      implicit none
      character*(*)switch
      character*(*) value
      logical failure
c
      integer iargc
      integer narg
      character*256 str
      character*256 opt
      integer oplen
      integer lnblnk
c
      integer argc
c
      narg = iargc()
c      opt(1:1)='-'
      oplen = lnblnk(switch)!+1
      opt(1:oplen)=switch(1:oplen)!-1)
      do argc = 1,narg
         call getarg(argc,str)
         if( str(1:lnblnk(str)) .eq. opt(1:oplen)) then
            call getarg(argc+1,value)
            if(lnblnk(value).eq.0)goto2
            failure=.false.
            return
         endif
      enddo
      failure = .true.
      return
 2    write(0,*)'Warning: failed to read character string after ',
     +          opt(1:oplen),' command line option'  
      failure = .true.
      return
      end


      subroutine lscancl(switch,value,failure)
      implicit none
      character*(*)switch
      logical value
      logical failure
c
      integer iargc
      integer narg
      character*256 str
      character*256 opt
      integer oplen
      integer lnblnk
c
      integer argc
c
      narg = iargc()
!      opt(1:1)='-'
      oplen = lnblnk(switch)!+1
      opt(1:oplen)=switch(1:oplen)!-1)
      value=.false.
      do argc = 1,narg
         call getarg(argc,str)
         if( str(1:lnblnk(str)) .eq. opt(1:oplen)) then
            value=.true.
            return 
         endif
      enddo
      failure = .false.
      return
      end





