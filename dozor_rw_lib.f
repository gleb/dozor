      subroutine dozor_input(X,D,templ,library)
      implicit none
      include 'dozor_param.fi'
      include 'dozor_dimension.fi'
      type (DETECTOR) D
      type (DATACOL)  X
      character*1024 templ
      character*(*) library
      integer first
      logical fail
      character*1024 string
c
      first=1
      call read_demand(X,first)    ! read command line
      call iscancl("-bin",D%binning_factor,fail)
      if(fail)then
          D%binning_factor=1
      else
          first =first+2
      endif
      call getarg(first,string)    ! read the file name where all data
      call read_dozor(D,X,string,templ,library)
      if(X%number_images.gt.nimage)then
         write(0,*) 'ERROR: Number of images can not be exceed',nimage
         call exit(-1)
      endif
      return
      end

      subroutine write_sum_int(X,Xp)
      implicit none
      include 'dozor_param.fi'
      include 'dozor_dimension.fi'
      type (DATACOL)    X 
      type (DATACOL_PICKLE) Xp(X%number_images)
      integer i
c
      open(12,file="dozor_sum_int.dat",status="unknown")
      WRITE(UNIT=12,fmt='(a)')
     +     'image totalInten totalBackgr  relDInten relInt/Int(1)'
      do i=1,X%number_images
         WRITE(UNIT=12,fmt='(i4,x,2f12.0,x,f8.4,x,f7.4)')
     +       i
     +       ,Xp(i)%SumTotal2D,Xp(i)%SumBack2D
     +       ,100*(1-Xp(i)%SumBack2D/Xp(i)%SumTotal2D)
     +       ,Xp(i)%SumTotal2D/Xp(1)%SumTotal2D
      enddo
      WRITE(UNIT=12,fmt='(a)')'end'
      close(unit=12)
      return
      end

      subroutine read_dozor(D,X,data_file,templ,library)
c===================================================================
!!                          +-------------------+
!!                          |   P U R P O S E   |
!!                          +-------------------+
!!   read parameters  
c====================================================================

!!
!!                 +-------------------------------------+
!!                 |   D E S C R I P T I V E   P A R T   |
!!                 +-------------------------------------+
      
      implicit none

      CHARACTER*1024 data_file
      CHARACTER*1024 string
      include 'dozor_param.fi'
      include 'dozor_dimension.fi'  
          
c      type (CRYSTAL)    C       ! CRYSTAL parameters            
      type (DETECTOR)   D       ! DETECTOR parameters 
      type (DATACOL)    X       ! D.C.    parameters
                             
      integer ctag,i,j,k
      integer ixcen,iycen
      real phiwidth
      real rdetmax,hmax,h2t  
      character*32 tag,tag1
      character*320 tag2    
      integer lnblnk
      real nofpic,nofback
      real dx
      CHARACTER*4 ntt      
      CHARACTER*1024 templ
      CHARACTER*(*) library

      real w2d(33),fl2d(33),fl2dS
      real flux,bsizeX,bsizeZ,fl2dt
!!               +-----------------------------------------+
!!               |   E X E C U T A B L E   S E C T I O N   |
!!               +-----------------------------------------+

!!
      flux=0.0  
      X%nbad=0
      X%wedge=1
      X%sigLev=5.0     
      OPEN(UNIT=1,FILE=data_file,STATUS='OLD',ERR=9)
           
      do k=1,100

         READ(UNIT=1,FMT='(a1024)',ERR=9,END=1)string

	 if(string(1:1).eq.'!')goto 2
	 
         j=ctag(1,string,tag1)

         if(j.ne.1)goto 98
	select case(tag1)

	 case('end')
	 goto 1	
	 
         case('nx')
           j=ctag(2,string,tag)
           if(j.ne.1)goto 98    
           read(tag,*,err=98)D%ix_unbinned

         case('ny') 
           j=ctag(2,string,tag)
           if(j.ne.1)goto 98
           read(tag,*,err=98)D%iy_unbinned

         case('pixel')
           j=ctag(2,string,tag)
           if(j.ne.1)goto 98
           read(tag,*,err=98)D%pixel

	 case('exposure')
	   j=ctag(2,string,tag)
           if(j.ne.1)goto 98	  
	   read(tag,*,err=98)X%texposure
	   if(X%texposure.le.0.)goto 99
	   	   
	 case('detector_distance')
	   j=ctag(2,string,tag)
           if(j.ne.1)goto 98	  
	   read(tag,*,err=98)X%dist
	   if(X%dist.le.0.)goto 99

	 case('X-ray_wavelength')
	   j=ctag(2,string,tag)
           if(j.ne.1)goto 98	  
	   read(tag,*,err=98)X%wave
	   if(X%wave.le.0.)goto 99
   
	 case('fraction_polarization')
	   j=ctag(2,string,tag)
           if(j.ne.1)goto 98	  
	   read(tag,*,err=98)X%monoch
	   if(X%monoch.le.0.)goto 99   
   
	 case('orgx')
	   j=ctag(2,string,tag)
           if(j.ne.1)goto 98	  
	   read(tag,*,err=98)X%xcen
	   
	 case('orgy')
	   j=ctag(2,string,tag)
           if(j.ne.1)goto 98	  
	   read(tag,*,err=98)X%ycen	   
	   
ccc crystal	   
	   
	 case('oscillation_range')
	   j=ctag(2,string,tag)
           if(j.ne.1)goto 98	  
	   read(tag,*,err=98)X%phiwidth	   	   	   
	   if(phiwidth.lt.0.)goto 99
   	   	   	   	   
	 case('starting_angle')
	   j=ctag(2,string,tag)
           if(j.ne.1)goto 98	  
	   read(tag,*,err=98)X%start_angl
	   
	 case('number_images')
	   j=ctag(2,string,tag)
           if(j.ne.1)goto 98	  
	   read(tag,*,err=98)X%number_images
	   if(X%number_images.lt.0.)goto 99
	   
	 case('first_image_number')
	   j=ctag(2,string,tag)
           if(j.ne.1)goto 98	  
	   read(tag,*,err=98)X%image_first
	   if(X%image_first.lt.0.)goto 99	   	    

	 case('name_template_image')
	   j=ctag(2,string,tag2)
           if(j.ne.1)goto 98
	   templ=tag2

         case('library')
           j=ctag(2,string,tag2)
           if(j.ne.1)goto 98
           library=tag2

	 case('pixel_min')
	   j=ctag(2,string,tag)
           if(j.ne.1)goto 98	  
	   read(tag,*,err=98)X%pixel_min

	 case('pixel_max')
	   j=ctag(2,string,tag)
           if(j.ne.1)goto 98	  
	   read(tag,*,err=98)X%pixel_max
	   
	 case('ix_max')
	   j=ctag(2,string,tag)
           if(j.ne.1)goto 98	  
	   read(tag,*,err=98)X%Kxmax
	   
	 case('ix_min')
	   j=ctag(2,string,tag)
           if(j.ne.1)goto 98	  
	   read(tag,*,err=98)X%Kxmin
	   
	 case('iy_max')
	   j=ctag(2,string,tag)
           if(j.ne.1)goto 98	  
	   read(tag,*,err=98)X%Kymax
	   
	 case('iy_min')
	   j=ctag(2,string,tag)
           if(j.ne.1)goto 98	  
	   read(tag,*,err=98)X%Kymin	   
	     	   
	 case('spot_size')
	   j=ctag(2,string,tag)
           if(j.ne.1)goto 98	  
	   read(tag,*,err=98)X%Ispot
	   
	 case('bad_zona')
	 X%nbad=X%nbad+1
	   j=ctag(2,string,tag)
           if(j.ne.1)goto 98	  
	   read(tag,*,err=98)X%Bxmin(X%nbad)	   
	   j=ctag(3,string,tag)
           if(j.ne.1)goto 98	  
	   read(tag,*,err=98)X%Bxmax(X%nbad)
	   j=ctag(4,string,tag)
           if(j.ne.1)goto 98	  
	   read(tag,*,err=98)X%Bymin(X%nbad)	   
	   j=ctag(5,string,tag)
           if(j.ne.1)goto 98	  
	   read(tag,*,err=98)X%Bymax(X%nbad)	
	      
	 case('wedge_number')
	   j=ctag(2,string,tag)
           if(j.ne.1)goto 98	  
	   read(tag,*,err=98)X%wedge
	   
	 case('spot_level')
	   j=ctag(2,string,tag)
           if(j.ne.1)goto 98	  
	   read(tag,*,err=98)X%sigLev	   	   	   
	   if(phiwidth.lt.0.)goto 99
           
         end select  	   
        goto 2
 
 1    exit          
 2    continue
         enddo
	 
      close(1) 
      if ( D%ix_unbinned * D%iy_unbinned * 
     +     X%xcen * X%ycen * D%pixel *
     +     X%dist * X%wave .eq. 0 ) then
          write(0,*)'ERROR: essential geometry parameter(s) '
     +              //'missing in the DATA_FILE'
          call exit(1)
      endif
      return        
        
!     ! ERROR MESSAGES

 9    write(0,*)' ERROR: in opening DATA_FILE'      
 98   write(0,*)' ERROR: in reading DATA_FILE'
      call EXIT(1)
 99   write(0,*)' ERROR: negative or zero value in DATA_FILE '
      call EXIT(1)      
       
      return
      end subroutine read_dozor


c----------------------------------------------------------------- 


      subroutine get_detetor_inf_full_name(file)
      implicit none
c
      character*1024 str
      character*(*) file
      integer ok
      integer lnblnk
c
      
      call getenv("besthome",str)
      if(lnblnk(str).eq.0)then
         write(0,*)"ERROR: besthome: undefined variable"
         call exit(-1)
      endif
c
c 
c     assemble full path to $besthome/detector_inf.dat by trial and error
c     - windows.....
c
c     try straight:
      open(1,file=file,status="old",iostat=ok)
      if(ok.ne.0)then !try unix
         file=trim(str)//"/detector-inf.dat"
         open(1,file=file,status="old",iostat=ok)
      endif
      if(ok.ne.0)then !try windows...
         call dequote(str)
         file=trim(str)//"\detector-inf.dat"
         open(1,file=file,status="old",iostat=ok)
      endif
      if(ok.ne.0)then
         write(0,*)"ERROR: file not found: (dir)"//trim(str)//
     +        " (file)detector-inf.dat"
         call exit(-1)
      endif 
c
      close(1)
      return
      end

      
      subroutine dequote(str)
      character*(*) str
      integer i,j
      integer lnblnk
c
      i=lnblnk(str)
      if(str(i:i).eq.'"')str(i:i)=" "
      if(str(1:1).eq.'"')then
         do j=1,i-1
            str(j:j)=str(j+1:j+1)
         enddo
      endif
      return
      end

c-begin-text BACKGROUND_PRINT
         SUBROUTINE  BACKGROUND_PRINT(D,X,Xp,LC)
!!                          +-------------------+
!!                          |   P U R P O S E   |
!!                          +-------------------+
c   program for the 3D background analyse and print
c ******************************************************************

!!
!!                 +-------------------------------------+
!!                 |   D E S C R I P T I V E   P A R T   |
!!                 +-------------------------------------+

      IMPLICIT NONE 

      include 'dozor_param.fi'  
      include 'dozor_dimension.fi'  
              
      type (DETECTOR)   D       ! DETECTOR parameters
c     type (CRYSTAL)    C       ! CRYSTAL parameters
      type (DATACOL)    X       ! D.C.    parameters
      type (DATACOL_PICKLE) Xp(X%number_images)  !  
      type (LOCAL)      LC      ! local parameters 
                 
      integer i,j 
      real sum1,sumt(X%number_images)    
      
!!               +-----------------------------------------+
!!               |   E X E C U T A B L E   S E C T I O N   |
!!               +-----------------------------------------+

c           if(X%g_backg)goto 1

      open(12,file='background_res.dat',status="unknown")
      WRITE(UNIT=12,fmt='(I4))')X%number_images	    
      WRITE(UNIT=12,fmt='(14x,2000(f10.3,x))')
     +(X%start_angl+(i-1)*X%phiwidth,i=1,X%number_images)  
      do j=0,nrbin
         WRITE(UNIT=12,fmt='(f6.3,x,f6.3,x,2000(f10.3,x))')
     +   1./sqrt((X%hmin2+(j-1)*X%delh2)),(X%hmin2+(j-1)*X%delh2),
     +   (Xp(i)%backpol2D(j),i=1,X%number_images)
      enddo
      close(unit=12)
    
1     continue    
               
      open(12,file='dozor_background.mtv',status="unknown")            
c Write file for Plotmtv
c ======================
 510          FORMAT(a)
      WRITE(UNIT=12,fmt=510)"$ DATA=CURVE2D"
      WRITE(UNIT=12,fmt=510)"% toplabel  = 'Background Plot'"

      WRITE(UNIT=12,fmt=510)"%xmin = 0"
      WRITE(UNIT=12,fmt=510)"% xlabel = '1/ Resolution**2'"
      WRITE(UNIT=12,fmt=510)"% ylabel = 'Intensity'"

      WRITE(UNIT=12,fmt=510)"# Curve 1"
      WRITE(UNIT=12,fmt=510)"% linetype   = 11"  
      WRITE(UNIT=12,fmt=510)"% linewidth  = 3"   
      WRITE(UNIT=12,fmt=510)"% linecolor  = 3"   
      WRITE(UNIT=12,fmt=510)"% linelabel  = 'background'"

      do i=1,nrbin
         WRITE(UNIT=12,fmt='(f5.3,2x,f10.4)')
     +  (X%hmin2+(i-1)*X%delh2),Xp(1)%backpol2D(i)
      enddo

      if(X%number_images.gt.1)then
         sum1=0.0
         do i=1,nrbin
            sum1=sum1+Xp(1)%backpol2D(i)
         enddo  
   
         do j=1,X%number_images
            sumt(j)=0.     
            do i=1,nrbin       
             sumt(j)=sumt(j)+Xp(j)%backpol2D(i)
            enddo       
         enddo   
         WRITE(UNIT=12,fmt=510)"$ DATA=CURVE2D"
         WRITE(UNIT=12,fmt=510)
     +   "% toplabel  = 'Background vs.rotation angle'"

         WRITE(UNIT=12,fmt=510)"%ymin = 0"        
         WRITE(UNIT=12,fmt=510)"% xlabel = 'Omega'"
         WRITE(UNIT=12,fmt=510)"% ylabel = 'Relative Intensity'"

         WRITE(UNIT=12,fmt=510)"# Curve 1"
         WRITE(UNIT=12,fmt=510)"% linetype   = 11"  
         WRITE(UNIT=12,fmt=510)"% linewidth  = 3"   
         WRITE(UNIT=12,fmt=510)"% linecolor  = 3"   
         do j=1,X%number_images
            WRITE(UNIT=12,fmt='(f7.3,2x,f6.3)')
     +      X%start_angl+(j-1)*X%phiwidth,sumt(j)/sum1
         enddo
      endif
      close(unit=12)
c      goto 10
c 99   write(0,*)'ERROR IN OPENING - backgraund.mtv'    
c      goto 11
c 11   call EXIT(1)    
c 10   continue
      return
      end
     
        subroutine write_spot_wilson_mtv(X,Xp,numb,
     +                        ixmax,iymax,
     +                        binning)

!!                          +-------------------+
!!                          |   P U R P O S E   |
!!                          +-------------------+
c   2D plot of finded spots and Wilson plot 
c ******************************************************************

!!
!!                 +-------------------------------------+
!!                 |   D E S C R I P T I V E   P A R T   |
!!                 +-------------------------------------+

      IMPLICIT NONE 
      include 'dozor_param.fi'
      include 'dozor_dimension.fi'  
      type (DATACOL)    X       ! D.C.    parameters 
      type (DATACOL_PICKLE) Xp  ! single frame in this context   
      integer kl,j,i
      integer numb
      integer*4 ixmax,iymax       
      integer nchenal  
      INTEGER binning
      character*14 sgraph_name
      character*16 wgraph_name
      integer  OMP_GET_THREAD_NUM
          
!!               +-----------------------------------------+
!!               |   E X E C U T A B L E   S E C T I O N   |
!!               +-----------------------------------------+
      if ( .not. X%graph ) return
      if ( Xp%NofR .le. 0 ) return

      sgraph_name = "00000_spot.mtv"
      wgraph_name = "00000_wilson.mtv"
      write(sgraph_name(1:5),'(I5.5)')numb
      write(wgraph_name(1:5),'(I5.5)')numb  
      nchenal = OMP_GET_THREAD_NUM()+15
               
              open(nchenal,file=sgraph_name,status="unknown")            
c Write file for Plotmtv
c ======================
 510          FORMAT(a)
      WRITE(UNIT=nchenal,fmt=510)"$ DATA=CURVE2D"        
      WRITE(UNIT=nchenal,fmt=510)"% toplabel  = 'SPOTS COORDINAT'"
      WRITE(UNIT=nchenal,fmt=510)"%xmin = 0"
      WRITE(UNIT=nchenal,fmt=510)"%ymin = 0" 
      
      WRITE(UNIT=nchenal,fmt='(a,x,i5)')"%xmax = ",ixmax*binning
      WRITE(UNIT=nchenal,fmt='(a,x,i5)')"%ymax = " ,iymax*binning      
           
      WRITE(UNIT=nchenal,fmt=510)"% xlabel = 'X------>'"
      WRITE(UNIT=nchenal,fmt=510)"% ylabel = '------->Y'"

      WRITE(UNIT=nchenal,fmt=510)"# Curve 1"
      WRITE(UNIT=nchenal,fmt=510)"% linetype   = 0"  
      WRITE(UNIT=nchenal,fmt=510)"% markertype = 2"   
      WRITE(UNIT=nchenal,fmt=510)"% markercolor  = 4"   
      WRITE(UNIT=nchenal,fmt=510)"% markersize  = 1"

           do kl=0,nrbin      
	     do j=1,INT(X%RList(kl,0))

	  WRITE(UNIT=nchenal,fmt='(i5,2x,i5)')         
     + binning *INT(X%hklKoor(kl,j,1)),
     + binning *INT((iymax-X%hklKoor(kl,j,2)))
 
	    enddo
	    enddo
	    
           close(unit=nchenal)
	   
	  if(Xp%table_suc)then
        open(nchenal,file=wgraph_name,status="unknown")
           
c Write file for Plotmtv
c ======================
      WRITE(UNIT=nchenal,fmt=510)"$ DATA=CURVE2D"
      WRITE(UNIT=nchenal,fmt=510)"% toplabel  = 'Wilson Plot'"
      
      WRITE(UNIT=nchenal,fmt=510)"%xmin = 0"
      WRITE(UNIT=nchenal,fmt=510)"% xlabel = '1/ Resolution**2'"
      WRITE(UNIT=nchenal,fmt=510)"% ylabel = Intensity"
      WRITE(nchenal,fmt='(a,a,i3,a)')
     +"% subtitle  =", 
     +"'image number = ",numb,"'"  
      WRITE(UNIT=nchenal,fmt=510)"# Curve 1"
      WRITE(UNIT=nchenal,fmt=510)"% linetype   = 11"  
      WRITE(UNIT=nchenal,fmt=510)"% linewidth  = 3"   
      WRITE(UNIT=nchenal,fmt=510)"% linecolor  = 3"   
      WRITE(UNIT=nchenal,fmt=510)"% linelabel  = 'Theory'"

                             do i=1,INT(X%Wil(1,701))
      WRITE(UNIT=nchenal,fmt='(f5.3,2x,f10.2)')
     + X%Wil(1,i),X%Wil(2,i)
                             enddo

      WRITE(UNIT=nchenal,fmt=510)"# Curve 4" 
      WRITE(UNIT=nchenal,fmt=510)"% linelabel  = 'Experiment'"
      WRITE(UNIT=nchenal,fmt=510)"% linetype   = 5"
      WRITE(UNIT=nchenal,fmt=510)"% linewidth  = 2"
      WRITE(UNIT=nchenal,fmt=510)"% linecolor  = 4"  
      WRITE(UNIT=nchenal,fmt=510)"% markertype = 10" 
      WRITE(UNIT=nchenal,fmt=510)"% markercolor= 4"    
                             do i=1,INT(X%Wil(1,701))
      WRITE(UNIT=nchenal,fmt='(f5.3,2x,f10.2)')
     + X%Wil(1,i),X%Wil(3,i)
                             enddo

            close(unit=nchenal)	   
	         endif
	   	    
         return
          end	 
	  	      

	  
       subroutine write_spot_list(X, templ, numb, NofR_real, binning)
!!                          +-------------------+
!!                          |   P U R P O S E   |
!!                          +-------------------+
c   write list of spots in file spot_name 
c ******************************************************************

!!
!!                 +-------------------------------------+
!!                 |   D E S C R I P T I V E   P A R T   |
!!                 +-------------------------------------+

      IMPLICIT NONE  

      include 'dozor_param.fi'
      include 'dozor_dimension.fi'      
      type (DATACOL)  X
      character*1024 templ, inf
      character*10 outf
                 
      integer lnblnk 
      integer kl,j
      INTEGER NofR_real   
      integer nchenal,numb                                      
      INTEGER binning   
      integer OMP_GET_THREAD_NUM
                            
!!               +-----------------------------------------+
!!               |   E X E C U T A B L E   S E C T I O N   |
!!               +-----------------------------------------+         	  
      if (.not. X%sprint) return
      if (NofR_real.le.0) return

        nchenal=OMP_GET_THREAD_NUM()+15
        call template_substitute_frame_number(templ,
     +                        inf, numb) 
        outf='00000.spot'
	write(outf(1:5),'(i5.5)')numb
          open(nchenal,file=outf,status="unknown")      
	  WRITE(UNIT=nchenal,fmt='(a)')inf(1:lnblnk(inf))
	  WRITE(UNIT=nchenal,fmt='(a,i5)')'N_of_spots=  ',NofR_real 
	  WRITE(UNIT=nchenal,fmt='(a,f7.2)')'omega=  ', 
     + X%start_angl+X%phiwidth*numb
                if(NofR_real.gt.0)then	       
           do kl=0,nrbin       
	     do j=1,INT(X%RList(kl,0))
	  WRITE(UNIT=nchenal,fmt='(i2,2x,f6.1,2x,f6.1,2x,f10.1,
     &	  2x,f10.1)')         
     + kl,binning*REAL(X%hklKoor(kl,j,1)),
     +binning*REAL(X%hklKoor(kl,j,2)),
     &X%Rlist(kl,j),SQRT(X%Rlist(kl,j)+(X%Ilimit(kl)/5)**2)
 
	    enddo
	    enddo
	                           endif	       
 
          
	      close(unit=nchenal)
         return
          end	 
	  
	  	      
cc  read_demand     	  
      subroutine read_demand(X,first)
      implicit none
      include 'dozor_param.fi'
      type (DATACOL)    X          
      logical fail,isthere,there 
      integer first
       
       X%graph=.false.
          call lscancl("-g",isthere,fail)
          if(isthere)then
          X%graph=.true.
	  first=1+first	  
	  endif 
	        
       X%sprint=.false.
          call lscancl("-p",isthere,fail)
          if(isthere)then
          X%sprint=.true.
	  first=1+first	  
	  endif        
       
       X%backg=.false.
          call lscancl("-b",isthere,fail)
          if(isthere)then
          X%backg=.true.
	  first=1+first	
	  endif    
	  
       X%w=.false.
          call lscancl("-w",isthere,fail)
          if(isthere)then
          X%w=.true.
	  first=1+first	  
	  endif 
	  
       X%wg=.false.
          call lscancl("-wg",isthere,fail)
          if(isthere)then
          X%wg=.true.
	  first=1+first	  
	  endif 	   
	  
       X%rd=.false.
          call lscancl("-rd",isthere,fail)
          if(isthere)then
          X%rd=.true.
	  first=1+first	  
	  endif 
	  
       X%isum=.false.
          call lscancl("-s",isthere,fail)
          if(isthere)then
          X%isum=.true.
	  first=1+first	  
	  endif 
	  
       X%prAll=.false.
          call lscancl("-pall",isthere,fail)
          if(isthere)then
          X%prAll=.true.
	  first=1+first	  
	  endif 	  	          
      
      return
      end      


        subroutine PLOT_aver
     +(Ph0,Phs,iw,
     + numb,nrefrT,intrefT,scaleT,BavT,ScoreT,ResTotal)

        INTEGER numb
      real, dimension(numb)  :: intrefT,scaleT,BavT,ScoreT,ResTotal       
      integer,dimension(numb)  ::nrefrT
	INTEGER i
      real Ph0,Phs
      integer iw
	
        open(12,file='dozor_average.mtv',status="unknown")            
c Write file for Plotmtv
c ======================
 510          FORMAT(a)
      WRITE(UNIT=12,fmt=510)"$ DATA=CURVE2D"
      WRITE(UNIT=12,fmt=510)
     +"% toplabel  = 'Spot number vs. Rot.Angle'"
      
c      WRITE(UNIT=12,fmt=510)"%xmin = 1"
      WRITE(UNIT=12,fmt=510)"%ymin = 0"      
      WRITE(UNIT=12,fmt=510)"% xlabel = 'Rot.Angle'"
      WRITE(UNIT=12,fmt=510)"% ylabel = Spot number"

      WRITE(UNIT=12,fmt=510)"# Curve 1"
      WRITE(UNIT=12,fmt=510)"% linetype   = 5"  
      WRITE(UNIT=12,fmt=510)"% linewidth  = 1"   
      WRITE(UNIT=12,fmt=510)"% linecolor  = 4"   
      WRITE(UNIT=12,fmt=510)"% linelabel  = 'N.of Spots'"
      WRITE(UNIT=12,fmt=510)"% markertype = 5" 
      WRITE(UNIT=12,fmt=510)"% markercolor= 6"    

                             do i=1,numb
      WRITE(UNIT=12,fmt='(f6.2,2x,i6)')
     + (Ph0+(i-1)*Phs*iw+iw*Phs*0.5),nrefrT(i) 
                             enddo
			     
       if(intrefT(1).ne.0)then
      WRITE(UNIT=12,fmt=510)"$ DATA=CURVE2D"
      WRITE(UNIT=12,fmt=510)
     +"% toplabel  = 'Av.Spot Intensity vs.Rot.Angle'"
      
c      WRITE(UNIT=12,fmt=510)"%xmin = 1"
      WRITE(UNIT=12,fmt=510)"%ymin = 0"      
      WRITE(UNIT=12,fmt=510)"% xlabel = 'Rot.Angle'"
      WRITE(UNIT=12,fmt=510)"% ylabel = Intensity"

      WRITE(UNIT=12,fmt=510)"# Curve 1"
      WRITE(UNIT=12,fmt=510)"% linetype   = 5"  
      WRITE(UNIT=12,fmt=510)"% linewidth  = 1"   
      WRITE(UNIT=12,fmt=510)"% linecolor  = 6"   
      WRITE(UNIT=12,fmt=510)"% linelabel  = 'rel.Intensity'"
      WRITE(UNIT=12,fmt=510)"% markertype = 10" 
      WRITE(UNIT=12,fmt=510)"% markercolor= 4"    

                             do i=1,numb
			     
      WRITE(UNIT=12,fmt='(f6.2,2x,f8.3)')
     + (Ph0+(i-1)*Phs*iw+iw*Phs*0.5),intrefT(i)/intrefT(1)   
                             enddo
	endif		     
	
	if(ScoreT(1).ne.0)then		     
      WRITE(UNIT=12,fmt=510)"$ DATA=CURVE2D"
      WRITE(UNIT=12,fmt=510)
     +"% toplabel  = 'Av.Wilson Intensity vs.Rot.Angle'"
      
c      WRITE(UNIT=12,fmt=510)"%xmin = 1"
      WRITE(UNIT=12,fmt=510)"%ymin = 0"         
      WRITE(UNIT=12,fmt=510)"% xlabel = 'Rot.Angle'"
      WRITE(UNIT=12,fmt=510)"% ylabel = intensity"

      WRITE(UNIT=12,fmt=510)"# Curve 1"
      WRITE(UNIT=12,fmt=510)"% linetype   = 5"  
      WRITE(UNIT=12,fmt=510)"% linewidth  = 1"   
      WRITE(UNIT=12,fmt=510)"% linecolor  = 3"   
      WRITE(UNIT=12,fmt=510)"% linelabel  = 'rel.Intensity'"
      WRITE(UNIT=12,fmt=510)"% markertype = 10" 
      WRITE(UNIT=12,fmt=510)"% markercolor= 4"    

                             do i=1,numb
      WRITE(UNIT=12,fmt='(f6.2,2x,f8.3)')
     + (Ph0+(i-1)*Phs*iw+iw*Phs*0.5),ScoreT(i)/ScoreT(1)   
                             enddo
	endif		     
			     
      WRITE(UNIT=12,fmt=510)"$ DATA=CURVE2D"
      WRITE(UNIT=12,fmt=510)
     +"% toplabel  = 'B-factor vs.Rot.Angle'"
      
c      WRITE(UNIT=12,fmt=510)"%xmin = 1"
      WRITE(UNIT=12,fmt=510)"%ymin = 0"       
      WRITE(UNIT=12,fmt=510)"% xlabel = 'Rot.Angle'"
      WRITE(UNIT=12,fmt=510)"% ylabel = B-factor"

      WRITE(UNIT=12,fmt=510)"# Curve 1"
      WRITE(UNIT=12,fmt=510)"% linetype   = 5"  
      WRITE(UNIT=12,fmt=510)"% linewidth  = 1"   
      WRITE(UNIT=12,fmt=510)"% linecolor  = 1"   
      WRITE(UNIT=12,fmt=510)"% linelabel  = 'B-factor'"
      WRITE(UNIT=12,fmt=510)"% markertype = 9" 
      WRITE(UNIT=12,fmt=510)"% markercolor= 5"    

                             do i=1,numb
      WRITE(UNIT=12,fmt='(f6.2,2x,f6.1)')
     + (Ph0+(i-1)*Phs*iw+iw*Phs*0.5),BavT(i)  
                             enddo			     
	if(scaleT(1).ne.0)then 		     
      WRITE(UNIT=12,fmt=510)"$ DATA=CURVE2D"
      WRITE(UNIT=12,fmt=510)
     +"% toplabel  = 'Scale vs.Rot.Angle'"
      
c      WRITE(UNIT=12,fmt=510)"%xmin = 1"
      WRITE(UNIT=12,fmt=510)"%ymin = 0"         
      WRITE(UNIT=12,fmt=510)"% xlabel = 'Rot.Angle'"
      WRITE(UNIT=12,fmt=510)"% ylabel = Scale"

      WRITE(UNIT=12,fmt=510)"# Curve 1"
      WRITE(UNIT=12,fmt=510)"% linetype   = 5"  
      WRITE(UNIT=12,fmt=510)"% linewidth  = 1"   
      WRITE(UNIT=12,fmt=510)"% linecolor  = 2"   
      WRITE(UNIT=12,fmt=510)"% linelabel  = 'Scale'"
      WRITE(UNIT=12,fmt=510)"% markertype = 8" 
      WRITE(UNIT=12,fmt=510)"% markercolor= 4"    

                             do i=1,numb
      WRITE(UNIT=12,fmt='(f6.2,2x,f8.3)')
     + (Ph0+(i-1)*Phs*iw+iw*Phs*0.5),scaleT(i)/scaleT(1) 
                             enddo
       endif
       
      WRITE(UNIT=12,fmt=510)"$ DATA=CURVE2D"
      WRITE(UNIT=12,fmt=510)
     +"% toplabel  = 'Resolution vs.Rot.Angle'"
      
c      WRITE(UNIT=12,fmt=510)"%xmin = 1"
      WRITE(UNIT=12,fmt=510)"%ymin = 0"         
      WRITE(UNIT=12,fmt=510)"% xlabel = 'Rot.Angle'"
      WRITE(UNIT=12,fmt=510)"% ylabel = Scale"

      WRITE(UNIT=12,fmt=510)"# Curve 1"
      WRITE(UNIT=12,fmt=510)"% linetype   = 5"  
      WRITE(UNIT=12,fmt=510)"% linewidth  = 1"   
      WRITE(UNIT=12,fmt=510)"% linecolor  = 2"   
      WRITE(UNIT=12,fmt=510)"% linelabel  = 'Resolution'"
      WRITE(UNIT=12,fmt=510)"% markertype = 8" 
      WRITE(UNIT=12,fmt=510)"% markercolor= 4"    

                             do i=1,numb
      WRITE(UNIT=12,fmt='(f6.2,2x,f8.3)')
     + (Ph0+(i-1)*Phs*iw+iw*Phs*0.5),ResTotal(i) 
                             enddo       
       			     				     
c      WRITE(UNIT=12,fmt=510)"$ END"
            close(unit=12)
               goto 10
 99    write(0,*)'ERROR IN OPENING - Av_Plot_mtv'    
         goto 11
 1000    write(0,*)iios,'ERROR IN clousing - mtv-file' 
 11      call EXIT(1)    
 10     continue
         return
          end
	  
c---------------------------------------------------------------------
        subroutine PLOT_rd
     +(tex,iw,numb,nrefrT,ScoreT,BavT)

        INTEGER numb
      real, dimension(numb)  :: ScoreT,Tt,Rref,BavT      
      integer,dimension(numb)  ::nrefrT
	INTEGER i
      real tex
      integer iw,numbt
      real b,c,T1,T2,T3
      real sigmaR,sigmaI,sigmaB,xt

      T1=ScoreT(1)
                             do i=1,numb
      Tt(i)= (tex*iw*(i-0.5)) 
      Rref(i)=Real( nrefrT(i)) 
      ScoreT(i)=ScoreT(i)/T1
                             enddo      
	
        open(12,file='dozor_rd.mtv',status="unknown")            
c Write file for Plotmtv
c ======================
 510          FORMAT(a)
 
 	call Qfit(numb,Tt,Rref,b,c,T1,sigmaR)
	 if(b.ne.0)then
	 xt=(Rref(1)*0.1-c)/b
	            endif
	numbt=INT(xt/(tex*iw)+0.5)

	if(numbt.gt.numb.or.numbt.le.0)numbt=numb	    
 	call Qfit(numbt,Tt,Rref,b,c,T1,sigmaR)		
	   
      WRITE(UNIT=12,fmt=510)"$ DATA=CURVE2D"
      WRITE(UNIT=12,fmt=510)
     +"% toplabel  = 'Spot number vs.Exposure'"
     
       WRITE(12,fmt='(a,a,f9.3,a,f9.3,a)')
     +"% subtitle  =", 
     +"'spots Half-dose Time =",T1, 
     +" s   sigma fit =",sigmaR,      
     +"'"        
      WRITE(UNIT=12,fmt=510)"%xmin = 0"
      WRITE(UNIT=12,fmt=510)"%ymin = 0"      
      WRITE(UNIT=12,fmt=510)"% xlabel = 'Exposure time, s'"
      WRITE(UNIT=12,fmt=510)"% ylabel = Spot number"

      WRITE(UNIT=12,fmt=510)"# Curve 1"
      WRITE(UNIT=12,fmt=510)"% linetype   = 5"  
      WRITE(UNIT=12,fmt=510)"% linewidth  = 1"   
      WRITE(UNIT=12,fmt=510)"% linecolor  = 4"   
      WRITE(UNIT=12,fmt=510)"% linelabel  = 'N.of Spots'"
      WRITE(UNIT=12,fmt=510)"% markertype = 5" 
      WRITE(UNIT=12,fmt=510)"% markercolor= 6"    

                             do i=1,numb
      WRITE(UNIT=12,fmt='(f6.2,2x,i6)')
     + Tt(i),nrefrT(i)   
                             enddo

      WRITE(UNIT=12,fmt=510)"# Curve 2"
      WRITE(UNIT=12,fmt=510)"% linetype   = 11"  
      WRITE(UNIT=12,fmt=510)"% linewidth  = 3"   
      WRITE(UNIT=12,fmt=510)"% linecolor  = 4"   
      WRITE(UNIT=12,fmt=510)"% linelabel  = 'Fit'"

                             do i=0,numb
      WRITE(UNIT=12,fmt='(f6.2,2x,f8.1)')
     + Tt(i),(b*Tt(i)+c)   
                             enddo	
		     
			     
	call Qfit(numb,Tt,ScoreT,b,c,T2,sigmaI)	
	
	 if(b.ne.0)then
	 xt=(ScoreT(1)*0.1-c)/b
	            endif
	numbt=INT(xt/(tex*iw)+0.5)

	if((numbt.gt.numb).or.(numbt.le.0))then
	numbt=numb
	else	    
 	call Qfit(numbt,Tt,ScoreT,b,c,T2,sigmaI)
	endif		
				     
      WRITE(UNIT=12,fmt=510)"$ DATA=CURVE2D"
      WRITE(UNIT=12,fmt=510)
     +"% toplabel  = 'Av.Wilson Intensity vs.Exposure'"
       WRITE(12,fmt='(a,a,f7.3,a,f9.3,a)')
     +"% subtitle  =", 
     +"'intensity Half-dose Time =",T2,
     + " s  sigma_fit =",sigmaI,      
     +"'"             
      
      WRITE(UNIT=12,fmt=510)"%xmin = 0"
      WRITE(UNIT=12,fmt=510)"%ymin = 0"         
      WRITE(UNIT=12,fmt=510)"% xlabel = 'Exposure time, s'"
      WRITE(UNIT=12,fmt=510)"% ylabel = intensity"

      WRITE(UNIT=12,fmt=510)"# Curve 1"
      WRITE(UNIT=12,fmt=510)"% linetype   = 5"  
      WRITE(UNIT=12,fmt=510)"% linewidth  = 1"   
      WRITE(UNIT=12,fmt=510)"% linecolor  = 3"   
      WRITE(UNIT=12,fmt=510)"% linelabel  = 'rel.Intensity'"
      WRITE(UNIT=12,fmt=510)"% markertype = 10" 
      WRITE(UNIT=12,fmt=510)"% markercolor= 4"    

                             do i=1,numb
      WRITE(UNIT=12,fmt='(f6.2,2x,f8.3)')
     + Tt(i),ScoreT(i) 
                             enddo

      WRITE(UNIT=12,fmt=510)"# Curve 2"
      WRITE(UNIT=12,fmt=510)"% linetype   = 11"  
      WRITE(UNIT=12,fmt=510)"% linewidth  = 3"   
      WRITE(UNIT=12,fmt=510)"% linecolor  = 4"   
      WRITE(UNIT=12,fmt=510)"% linelabel  = 'Fit'"

                             do i=0,numb
      WRITE(UNIT=12,fmt='(f6.2,2x,f8.3)')
     + Tt(i),(b*Tt(i)+c)   
                             enddo
			     
	call Qfit(numb,Tt,BavT,b,c,T3,sigmaB)				     
			     
      WRITE(UNIT=12,fmt=510)"$ DATA=CURVE2D"
    
      WRITE(UNIT=12,fmt=510)
     +"% toplabel  = 'B-factor vs.Exposure'"
       WRITE(12,fmt='(a,a,f7.3,a,a)')
     +"% subtitle  =", 
     +"'B-factor 10 ² Time increase  =",10/b, " s ", 
     +"'"              
      
      WRITE(UNIT=12,fmt=510)"%xmin = 0"
      WRITE(UNIT=12,fmt=510)"%ymin = 0"         
      WRITE(UNIT=12,fmt=510)"% xlabel = 'Exposure time, s'"
      WRITE(UNIT=12,fmt=510)"% ylabel = intensity"

      WRITE(UNIT=12,fmt=510)"# Curve 1"
      WRITE(UNIT=12,fmt=510)"% linetype   = 5"  
      WRITE(UNIT=12,fmt=510)"% linewidth  = 1"   
      WRITE(UNIT=12,fmt=510)"% linecolor  = 3"   
      WRITE(UNIT=12,fmt=510)"% linelabel  = 'B-factor'"
      WRITE(UNIT=12,fmt=510)"% markertype = 10" 
      WRITE(UNIT=12,fmt=510)"% markercolor= 4"    

                             do i=1,numb
      WRITE(UNIT=12,fmt='(f6.2,2x,f8.3)')
     + Tt(i),BavT(i) 
                             enddo
	
      WRITE(UNIT=12,fmt=510)"# Curve 2"
      WRITE(UNIT=12,fmt=510)"% linetype   = 11"  
      WRITE(UNIT=12,fmt=510)"% linewidth  = 3"   
      WRITE(UNIT=12,fmt=510)"% linecolor  = 4"   
      WRITE(UNIT=12,fmt=510)"% linelabel  = 'Fit'"

                             do i=0,numb
      WRITE(UNIT=12,fmt='(f6.2,2x,f8.3)')
     + Tt(i),(b*Tt(i)+c)   
                             enddo
			     			     				     
			     

            close(unit=12)
	    
            WRITE
     +(UNIT=6,fmt='(/,a,3(F8.3,x))')
     +'Rad.Damage Half-dose Time=',T2 
     
            WRITE      
     +(UNIT=6,fmt='(a,3(F8.3,x))')
     +'                and Sigma=',sigmaI      
	    
               goto 10
 99    write(0,*)'ERROR IN OPENING - Av_Plot_mtv'    
         goto 11
 1000    write(0,*)iios,'ERROR IN clousing - mtv-file' 
 11      call EXIT(1)    
 10     continue
         return
          end
	  
c----------------------------------------------------------
        subroutine   Qfit(numb,X,Y,b,c,Tp,sigma)    
        INTEGER numb
      real, dimension(numb)  :: X,Y
      real b,c,Tp,sigma
      integer i
      
      real Sxy,Sx,Sy,Sx2
      
        Sxy=0.
	Sx=0.
	Sy=0.
	Sx2=0.
	
             do i=1,numb
	   Sxy=Sxy+X(i)*Y(i) 
	   Sx=Sx+X(i)
	   Sy=Sy+Y(i)
	   Sx2=Sx2+X(i)**2
	     enddo
	     
	     b=(Sxy-Sx*Sy/numb)/(Sx2-Sx**2/numb)
	     c=(Sy-b*Sx)/numb
	   if(b.ne.0.)then   
           Tp=-0.5*c/b
	             else
            Tp=1e6
	             endif  		        
	   
	   sigma=0.
            do i=1,numb 
	  sigma= sigma+(X(i)*b+c -Y(i))**2
	    enddo
	   sigma= abs(Tp*SQRT(sigma/(numb-1))/(X(numb)*b))  
         return
          end  
	

  
      subroutine results_print_footer(X)
      implicit none
      include 'dozor_param.fi'
      type (DATACOL)    X
c
      IF( X%prAll )then
         WRITE
     +   (UNIT=6,fmt='(a,a)')
     +   '----------------------------------------------------',
     +   '----------------------------------------------------'
      else
          WRITE
     +   (UNIT=6,fmt='(a)')
     +   '------------------------------------'
      endif
      return
      end
 
      subroutine results_print_header(X)
      implicit none
      include 'dozor_param.fi'
      type (DATACOL)    X
c
      IF( X%prAll )then
         WRITE
     +   (UNIT=6,fmt='(a,a,a)')
     +   ' N    |            SPOTS             |',
     +   '        Powder Wilson        ',
     +   '      |        Main    Spot   Visible'
         WRITE
     +   (UNIT=6,fmt='(a,a,a)')
     +   'image | num.of  INTaver R-factor Res.|',
     +   '   Scale B-fac. Res. Corr.',
     +   ' R-factor|       Score   Score  Resolution'

         WRITE
     +   (UNIT=6,fmt='(a,a)')
     +   '----------------------------------------------------',
     +   '----------------------------------------------------'
      else
         WRITE
     +   (UNIT=6,fmt='(a)')
     +   ' N    | SPOTS     Main     Visible'

         WRITE
     +   (UNIT=6,fmt='(a)')
     +   'image | num.of   Score    Resolution'

         WRITE
     +   (UNIT=6,fmt='(a)')
     +   '------------------------------------'
      endif
      return
      end

       

      subroutine results_print_line(numb,X,Xp)
      implicit none
      include 'dozor_param.fi'
      type (DATACOL)    X
      type (DATACOL_PICKLE)    Xp ! single frame in this context
      integer numb
c
      IF( X%prAll )then      
         if(Xp%table_suc)then
            WRITE
     +         (UNIT=6,fmt='(i5,a,i6,x,f9.0,2x,f6.3,2x,f4.1,a,
     +         f8.2,x,f6.1,x,f4.1,x,f5.1,3x,f5.1,a,f12.3
     +         ,x,f7.2,3x,f5.2
     +         ))')
     +         numb,' |',Xp%NofR,Xp%Iav,Xp%Rfexp,
     +         Xp%dlim,'|',
     +         Xp%table_sc,Xp%table_b,
     +         Xp%table_resol,
     +         Xp%table_corr*100.,Xp%table_rfact*100.,
     +         ' |',Xp%Score3,Xp%Score2,Xp%dlim09
         else
c              Xp%table_est=0.0  ! why?
            WRITE
     +         (UNIT=6,fmt='(i5,a,i6,x,f9.0,2x,f6.3,2x,f4.1,a,
     +         a,f12.3,x,f7.2,3x,f5.2)')
     +         numb,' |',Xp%NofR,Xp%Iav,Xp%Rfexp,Xp%dlim
     +         ,'|',' ---------no results -----------   |',
     +         Xp%Score3,Xp%Score2,Xp%dlim
         endif
      else
         if(Xp%Score3.gt.0.)then
            WRITE
     +         (UNIT=6,fmt='(i5,a,i6,x,f9.2,3x,f5.2)')
     +         numb,' |',Xp%NofR,Xp%Score3,Xp%dlim09
         else
            WRITE
     +         (UNIT=6,fmt='(i5,a,i6,x,f9.2,3x,f5.2)')
     +         numb,' |',Xp%NofR,Xp%Score3
         endif
      endif
      return
      end

      subroutine results_print(X,Xp)
      include 'dozor_param.fi'
      type (DATACOL)    X
      type (DATACOL_PICKLE)    Xp(X%number_images)
c
      integer i
c
      call results_print_header(X)
      do i = 1, X%number_images
         call results_print_line(X%image_first+i-1,X,Xp(i))
      enddo
      call results_print_footer(X)
      return
      end

      SUBROUTINE results_print_average(X,Xp)
!!                          +-------------------+
!!                          |   P U R P O S E   |
!!                          +-------------------+
c   RESULT OUT
c
!!
!!                 +-------------------------------------+
!!                 |   D E S C R I P T I V E   P A R T   |
!!                 +-------------------------------------+

      IMPLICIT NONE 
      include 'dozor_param.fi' 
      include 'dozor_dimension.fi'  
             
c      type (DETECTOR)   D       ! DETECTOR parameters
c      type (CRYSTAL)    C       ! CRYSTAL parameters
      type (DATACOL)    X       ! D.C.    parameters
      type (DATACOL_PICKLE) Xp(X%number_images)  
c      type (LOCAL)      LC      ! local parameters      
      
      integer ii,i,numb
      
      real Score3
      integer l3,k,iw,nrefr  
      real intref,scale,Bav,Dav,ResTot 
      real, dimension(nimage)  :: intrefT,scaleT,BavT,ScoreT 
      real, dimension(nimage)  :: ResTotal            
      integer,dimension(nimage)  ::nrefrT
      real*8, dimension(nimage)  :: SumTotal 
      real*8  SumTot                
c=================================================================
c-------------------------------------------------------------------
!!               +-----------------------------------------+
!!               |   E X E C U T A B L E   S E C T I O N   |
!!               +-----------------------------------------+

c  average data	-------------------------    
      if(X%w.or.X%wg.or.X%rd)then
         open(13,file='dozor_average.dat',status="unknown")
         WRITE
     +   (UNIT=13,fmt='(a,a)')
     +   ' N    |       SPOTS          |',     
     +   '        Powder Wilson        '   
         WRITE    
     +   (UNIT=13,fmt='(a,a,a)')
     +   'wedge | num.of  INTaver Res  |',     
     +   '   Scale B-fac.    AverIntens  Resolution' 

   
          WRITE
     +    (UNIT=13,fmt='(a,a)')
     +    '----------------------------------------------------',
     +    '----------------------------------------------------' 
          numb=0
         do ii=1,X%number_images,X%wedge
            Score3 =0
            nrefr=0
            intref=0.
            scale=0
            Bav=0
            iw=0
            Dav=0
            SumTot=0.
            ResTot=0
            do i=ii,ii+X%wedge -1
               if(Xp(i)%table_suc)then
                  iw=iw+1
                  nrefr =nrefr+Xp(i)%NofR
                  intref=intref+ Xp(i)%Iav
                  scale=scale+ Xp(i)%table_sc
                  Bav= Bav+Xp(i)%table_b
                  Score3 =Score3 +Xp(i)%table_est
c     +                  *(100./Xp(i)%Coef)
                  Dav=Dav+Xp(i)%dlim
                  SumTot=SumTot+Xp(i)%SumTotal2D
                  ResTot=ResTot+Xp(i)%dlim
               endif
            enddo
            numb=numb+1
            if(iw.ne.0)then
               nrefrT(numb) =nrefr
               intrefT(numb)=intref/iw
               scaleT(numb)=scale/iw
               BavT(numb)= Bav/iw
               ScoreT(numb) =Score3/iw
               Dav=Dav/iw
               SumTotal(numb)=SumTot/iw
               ResTotal(numb)=ResTot/iw
            else
               nrefrT(numb) =0
               intrefT(numb)=0
               scaleT(numb)=0
               BavT(numb)= 0
               ScoreT(numb) =0
               Dav=0
               SumTotal(numb)=1
               ResTotal(numb)=0
            endif
            WRITE
     +      (UNIT=13,fmt='(i5,a,i6,x,f9.0,x,f5.2,a,
     +      f8.2,x,f6.1,x,f12.3,x,f5.2)')
     +      numb,'  ',nrefrT(numb),intrefT(numb),Dav,' ',
     +      scaleT(numb),BavT(numb),ScoreT(numb),ResTotal(numb)       
         enddo 
         close(13)
      endif

      if(X%rd)then
         do i=1,numb
            ScoreT(i)=ScoreT(i)*SumTotal(1)/SumTotal(i)
c               print*,SumTotal(1)/SumTotal(i)
         enddo
         call PLOT_rd(X%texposure,X%wedge,
     +       numb,nrefrT,ScoreT,BavT)
      endif
                 
      if(X%wg)call PLOT_aver(X%start_angl,X%phiwidth,X%wedge,
     +   numb,nrefrT,intrefT,scaleT,BavT,ScoreT,ResTotal)
      return
      END
  
