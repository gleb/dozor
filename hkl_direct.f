!
      subroutine HKL_direct(image1,D,X,Xp,LC,PSIim,KLim)
 

!!                          +-------------------+
!!                          |   P U R P O S E   |
!!                          +-------------------+
!!   seach of diffraction intensities and background directly from the image

!!
!!                 +-------------------------------------+
!!                 |   D E S C R I P T I V E   P A R T   |
!!                 +-------------------------------------+

      implicit none      
      include 'dozor_param.fi'
      include 'dozor_dimension.fi'      
      type (DETECTOR) D
c      type (CRYSTAL)  C
      type (DATACOL)  X
      type (LOCAL)    LC      ! local parameters       
      type (DATACOL_PICKLE) Xp  ! intermediates for omp 
                 
      integer*4 IMSIZE          ! image size
c     image1 - vector of image (ix*iy)
c ================================
      integer*2 image1(D%ix*D%iy)
c      integer*4, ALLOCATABLE :: imagePeak(:)

      integer*1 PSIim(D%ix*D%iy), KLim(D%ix*D%iy)

c                      
c
c       XDS-FILE PARAMETERS
C      =================== 

      REAL*4 scale_xds

c         spliting on resolution and psi bins  
c         ==================================/
c######   low resolution = 12A 
c#####>    N.of resolution's bins = nrbin
c#####>    N.of psi's bins = 60    

      INTEGER nrefr
      REAL dpsi
      
c curent parameters for spots removing
c ====================================

      INTEGER*4 koor,koormax
      INTEGER i,j,k

c curent parameters for calculation of background distibution vs. 2D bins
c ======================================================================
      INTEGER,dimension(0:nrbin,1:npsibin)::nbct,nbctP
      REAL*8,dimension(0:nrbin,1:npsibin)::backtabl,backtablP
    
c Corrections on polarisation, absorbtion factor and detector's background
c ============================================================

      REAL, dimension(0:nrbin,1:npsibin)::sumerrd

      REAL*8 sumIP      
      real,dimension(0:nrbin)::backpol,backpolP,backerr,dspot
        
      INTEGER,dimension (0:nrbin,nspot) ::hPsi
                           
      INTEGER NofR      
      INTEGER NofR_real !n.of finded reflection  
      REAL Rfexp   
      logical redus(nrbin)
      integer ist
      REAL  Hlim2,Iav_best  
      real correct
      real*8 SumTotal,SumBack
      logical  BigBg  
      real sretio
      integer ks 
      integer ns1,ns2,ja
      real sall,sbg

   
       
!!               +-----------------------------------------+
!!               |   E X E C U T A B L E   S E C T I O N   |
!!               +-----------------------------------------+

      imsize=D%ix*D%iy

c      ALLOCATE ( imagePeak(imsize))

      ist=INT(X%Ispot/2)
       
      koormax=D%ix*D%iy 

      scale_xds =1.0 

C**** step of psi
c     ===================
      dpsi=pi/(0.5*npsibin)
 
      SumTotal=0
      SumBack =0    
c___________________________________________________________________
      call backg_extract_hist(
     &              X,imsize,
     &              PSIim,KLim,
     &              image1,
     &              backtabl,
     &              backtablP,
     &              sumerrd,
     &              nbct,nbctP,
     &              BigBg)

        if(BigBg)
     & call backg_extract(
     &              X,imsize,
     &              PSIim,KLim,
     &              image1,
     &              backtabl,
     &              backtablP,
     &              sumerrd,
     &              nbct,nbctP)
c     ==================================================
	    					   
       correct=3./(5*X%Ispot)  ! correct error --minimal spot intensity must be 3
       
       do i=1,nrbin
          do j=1,npsibin      
             if( sumerrd(i,j).lt.correct)sumerrd(i,j)=correct 
          enddo
       enddo
  
        do i=0,nrbin
           do j=1,npsibin
              SumTotal= SumTotal+backtablP(i,j)*nbctP(i,j)
              SumBack=SumBack + backtabl(i,j)*nbctP(i,j)
           enddo 
         enddo  
     
       call spots_seach(
     &              X,D,
     &              imsize,
     &              ist,
     &              PSIim,
     &              KLim,
     &              image1,
     &              backtabl,
     &              sumerrd,
     &              hPsi,
     &              dspot,
     &              NofR)       

C Correction on polarisation factor and detector's background
c ===========================================================
2      call backg_average(
     &              X,LC,
     &              backtabl,
     &              backtablP,
     &              nbct,
     &              sumerrd,
     &              backpol,
     &              backpolP,
     &              backerr)
c ============================================================    	       
      call ice_find(X,redus)  !!!posible Ice and solt rings detection 
c ==================================================================
      do i=1,nrbin
c      print*,redus(i)
       if(redus(i).eqv..FALSE.)then
            X%backpolP(i)=0.
            X%RList(i,0)=-1
         endif           
      enddo       
cpppppppppppppppppppppppppppppppppppppppppppppppppppppppppp	
      call  spots_analysis(
     &               X,LC,
     &               hpsi,
     &               dspot,
     &               redus,
     &               sumerrd,
     &               NofR_real,
     &               Rfexp,
     &               Iav_best,
     &               Hlim2,
     &               sumIP)
     
cttttttttttttttttttttttttttttttttttttttttttttttttttttt	
c      DEALLOCATE ( imagePeak)
      if(NofR_real.gt.0)then
         NofR_real=0 
         do i=0,nrbin
            If(X%RList(i,0).gt.0)
     +         NofR_real= NofR_real+INT(X%RList(i,0))
         enddo
      endif

c      sumIP=1000.
c      print*,sumIP
      do i=0,nrbin
         X%backpolP(i)=X%backpolP(i)*sumIP
         X%backerr(i)=X%backerr(i)*sumIP
c	 print*,i,X%backpolP(i),X%backerr(i)
      enddo
   
ccc   WILSON 
c============
      Xp%NofR =NofR_real
      Xp%Rfexp =Rfexp
      Xp%Iav =Iav_best
      Xp%dlim=1/Sqrt(Hlim2)
      Xp%dlim09=Xp%dlim*0.9
      Xp%Coef=sumIP

      if(NofR_real.eq.0)then
          Xp%table_suc=.false.
      else     
          call WILSON_single(D,X,Xp,LC)
      endif

      do j=0,nrbin       
         Xp%backpol2D(j) =X%backpol(j)
      enddo 
    
      Xp%SumTotal2D=SumTotal
      Xp%SumBack2D=SumBack

      return         
      end
      
c==================================================================
      real*8 function image1_to_imager(image1,X)
      implicit none
      include 'dozor_param.fi'
      type (DATACOL) X
      integer*2 image1
c
      if(image1.gt.X%pixel_max.or.image1.lt.X%pixel_min)then
          image1_to_imager=-1d0
      else
          image1_to_imager=DBLE(image1)
      endif
      return
      end
c

       subroutine backg_extract_hist
     & (X,imsize,
     &  PSIim,KLim,image1,
     &  backtabl,backtablP,sumerrd,
     &  nbct,nbctP,BigBg)
!!                          +-------------------+
!!                          |   P U R P O S E   |
!!                          +-------------------+              
c     calculation of background distribution vs. 2d bins (histogram method)
c     ======== ==================================================
!!
!!                 +-------------------------------------+
!!                 |   D E S C R I P T I V E   P A R T   |
!!                 +-------------------------------------+
      implicit none      
      include 'dozor_param.fi'
      include 'dozor_dimension.fi'      
      type (DATACOL)  X
       
      integer*4 IMSIZE          ! image size
      integer*1 PSIim(imsize), KLim(imsize)      
      integer*2 image1(imsize)       
      
c curent parameters for calculation of background distibution vs. 2D bins
c ======================================================================
      INTEGER,dimension(0:nrbin,1:npsibin)::nbct,nbctP
      REAL*8,dimension(0:nrbin,1:npsibin)::backtablt,backtablt2
      REAL*8,dimension(0:nrbin,1:npsibin):: backtabl,backtabl2
      REAL*8 ,dimension(0:nrbin,1:npsibin)::backtablP   
      REAL*8 imager,image1_to_imager 
      REAL ,dimension(0:nrbin,1:npsibin)::sumerrd
      INTEGER  ,dimension(0:nrbin,1:npsibin)::limerr   
         
c      INTEGER*4,dimension(1:nrbin,1:npsibin,0:100):: hist  
      
      INTEGER*4, allocatable :: hist(:,:,:)

      INTEGER*4 koor      
      integer i,j,l,ii
      integer kl,klpsi         
      logical  BigBg          
      allocate(hist(0:nrbin,1:npsibin,0:100))
       
         
!!               +-----------------------------------------+
!!               |   E X E C U T A B L E   S E C T I O N   |
!!               +-----------------------------------------+  
       BigBg =.false.
    
         do i=0,nrbin
            do j=1,npsibin
	       nbctP(i,j)=0  	       .       
	       backtablP(i,j)=0
		   limerr(i,j)=0	       
               nbct(i,j)=0      
               backtablt(i,j)=0.
               backtablt2(i,j)=0.
               backtabl(i,j)=0.	       
	       backtabl2(i,j)=0
	       sumerrd(i,j)=0
	       
		   do l=0,100
		   hist(i,j,l)=0
		   enddo	    	       
	       		   
            enddo
         enddo 	
		 
		      do koor=1,imsize,1
c========================================================
           IF(PSIim(koor).gt.0.and.KLim(koor).ge.0)then
                    klpsi=PSIim(koor)	    
		    kl   =KLim(koor)		    	       	       
               imager=image1_to_imager(image1(koor),X)
               if(imager.ge.X%pixel_min)then
                    
		  if(imager.le.100)then          	                               
            nbct(kl,klpsi)=nbct(kl,klpsi)+1
         backtablt(kl,klpsi)=backtablt(kl,klpsi) 
     +                             + imager     
         backtablt2(kl,klpsi)=backtablt2(kl,klpsi)
     +                             + imager**2
                                   endif
     
             nbctP(kl,klpsi)=nbctP(kl,klpsi)+1	  
          backtablP(kl,klpsi)=backtablP(kl,klpsi) 
     +                             + imager

	 l=imager
	     if(l.ge.100)l=100	    
	 hist(kl,klpsi,l)=hist(kl,klpsi,l)+1 
	  		   
                                       endif
			
						   endif
		       enddo								
c=====================================================================
			
         do i=0,nrbin
            do j=1,npsibin 
	    		     
c total intensity inside shells  
                  if(nbctP(i,j).ne.0)
     +     backtablP(i,j)=backtablP(i,j)/nbctP(i,j) 
                                   
               if(nbct(i,j).ne.0)then                    
                  backtabl(i,j)=backtablt(i,j)/nbct(i,j)
                  backtabl2(i,j)=backtablt2(i,j)/nbct(i,j)		  
                  sumerrd(i,j)=Sqrt(backtabl2(i,j)-backtabl(i,j)**2)
		                endif 
           enddo
	     enddo
	     				
c estimate background
                     do ii=1,3
		     
         do i=0,nrbin
            do j=1,npsibin  

            if(backtabl(i,j).lt.11)then
                l=Int(backtabl(i,j)*100.+0.5)
		     if(l.gt.1100)l=1100
	        limerr(i,j)=X%pLim1(l)
                                   else
		limerr(i,j)= Int(backtabl(i,j)+3*SQRT(backtabl(i,j)))		     
                                   endif
	   
	if(limerr(i,j).lt.(3*sumerrd(i,j)+backtabl(i,j)))
     +   limerr(i,j)=Int(3*sumerrd(i,j)+backtabl(i,j))
     
         if(limerr(i,j).ge.100)limerr(i,j)= 100 ! was 99; which lead to unassigned BigBg and all zeroes background on images with all pixels >= 100 counts 
    
	 backtablt(i,j)=0
	 backtablt2(i,j)=0
	 nbct(i,j)=0

	         do l=0,limerr(i,j)
	 nbct(i,j)= nbct(i,j)+hist(i,j,l)
	 backtablt(i,j)=backtablt(i,j)+	l*hist(i,j,l)
	 backtablt2(i,j)=backtablt2(i,j)+l**2*hist(i,j,l)
	         enddo	
		  
               if(nbct(i,j).ne.0)then  	 		  
            backtabl(i,j)=backtablt(i,j)/nbct(i,j)
            backtabl2(i,j)=backtablt2(i,j)/nbct(i,j)		  
            sumerrd(i,j)=Sqrt(backtabl2(i,j)-backtabl(i,j)**2)
	            if(ii.eq.3)then
	      if(backtabl(i,j).ge.72)BigBg =.true.
	                  endif	             
                                endif 	
							 
            enddo  
         enddo
	 
                       enddo		       			
      deallocate(hist)
      return         
      end

      subroutine backg_extract
     & (X,imsize,
     &  PSIim,KLim,image1,
     &  backtabl,backtablP,sumerrd,
     &  nbct,nbctP)
!!                          +-------------------+
!!                          |   P U R P O S E   |
!!                          +-------------------+              
c     calculation of background distribution vs. 2d bins - direct 
c     ======== ==================================================
!!
!!                 +-------------------------------------+
!!                 |   D E S C R I P T I V E   P A R T   |
!!                 +-------------------------------------+
      implicit none      
      include 'dozor_param.fi'
      include 'dozor_dimension.fi'      
      type (DATACOL)  X
       
      integer*4 IMSIZE          ! image size
      integer*1 PSIim(imsize), KLim(imsize)      
      integer*2 image1(imsize)       
      
c curent parameters for calculation of background distibution vs. 2D bins
c ======================================================================
      INTEGER,dimension(0:nrbin,1:npsibin)::nbct,nbctP
      REAL*8,dimension(0:nrbin,1:npsibin)::backtablt,backtablt2
      REAL*8,dimension(0:nrbin,1:npsibin):: backtabl,backtabl2
      REAL*8 ,dimension(0:nrbin,1:npsibin)::backtablP   
      REAL*8 imager,image1_to_imager
      REAL ,dimension(0:nrbin,1:npsibin)::sumerrd
      INTEGER ,dimension(0:nrbin,1:npsibin)::limerr  
               
      INTEGER*4 koor      
      integer i,j,l,ii
      integer kl,klpsi         
       
          
!!               +-----------------------------------------+
!!               |   E X E C U T A B L E   S E C T I O N   |
!!               +-----------------------------------------+  
 
   
         do i=0,nrbin
            do j=1,npsibin
	       nbctP(i,j)=0  	       .       
	       backtablP(i,j)=0
		   limerr(i,j)=0	       
            enddo
         enddo 
c         -------------------------------------
                         do ii=1,3
         do i=0,nrbin
            do j=1,npsibin
               nbct(i,j)=0      
               backtablt(i,j)=0.
               backtablt2(i,j)=0.
               backtabl(i,j)=0.	       
	       backtabl2(i,j)=0
	       sumerrd(i,j)=0		   
            enddo
         enddo 			 
                      
		      do koor=1,imsize,1
c========================================================
             
           IF(PSIim(koor).gt.0.and.KLim(koor).ge.0)then
                    klpsi=PSIim(koor)	    
		    kl   =KLim(koor)		    	       	       
               
c              imager=DBLE(image1(koor))
               imager=image1_to_imager(image1(koor),X)      
		   	If(ii.eq.1)then	
							
          if(imager.ne.-1d0)then
     
          nbctP(kl,klpsi)=nbctP(kl,klpsi)+1
          backtablP(kl,klpsi)=backtablP(kl,klpsi) 
     +                             + imager
     
         nbct(kl,klpsi)=nbct(kl,klpsi)+1
         backtablt(kl,klpsi)=backtablt(kl,klpsi) 
     +                             + imager
         backtablt2(kl,klpsi)=backtablt2(kl,klpsi)
     +                             + imager**2     							    
							      
			                                    endif
							    
				    else			    							    
							    		   
           if
     +(imager.ge.X%pixel_min.and.imager.le.limerr(kl,klpsi))then
                      	                               
         nbct(kl,klpsi)=nbct(kl,klpsi)+1
         backtablt(kl,klpsi)=backtablt(kl,klpsi) 
     +                             + imager
         backtablt2(kl,klpsi)=backtablt2(kl,klpsi)
     +                             + imager**2

	  		   
                                                             endif
			
				   endif
				   endif
			   enddo						
c=====================================================================
			
         do i=0,nrbin
            do j=1,npsibin 
	    
		   	   If(ii.eq.1)then		     
c total intensity inside shells  
                  if(nbctP(i,j).ne.0)
     +            backtablP(i,j)=backtablP(i,j)/nbctP(i,j) 
                                     endif 
     
               if(nbct(i,j).ne.0)then  
c estimate background	                  
                  backtabl(i,j)=backtablt(i,j)/nbct(i,j)
                  backtabl2(i,j)=backtablt2(i,j)/nbct(i,j)		  
                  sumerrd(i,j)=Sqrt(backtabl2(i,j)-backtabl(i,j)**2)
		                endif 
				
		If(ii.le.2)then		
            if(backtabl(i,j).le.11)then
	    
                   l=Int(backtabl(i,j)*100.+0.5)
		   if(l.gt.1100)l=1100
		   limerr(i,j)=X%pLim1(l)
		   
                                   else
		limerr(i,j)= Int(backtabl(i,j)+3*SQRT(backtabl(i,j)))		     
                                   endif
	   
	if(limerr(i,j).lt.(3*sumerrd(i,j)+backtabl(i,j)))
     +   limerr(i,j)=Int(3*sumerrd(i,j)+backtabl(i,j))
                          endif
     				         
           enddo
	     enddo

                       enddo
      return         
      end

c-begin-text spots_seach
!!
      subroutine spots_seach(
     &                    X,D,
     &                    imsize,
     &                    ist,
     &                    PSIim,
     &                    KLim,
     &                    image1,
     &                    backtabl,
     &                    sumerrd,
     &                    hPsi,
     &                    dspot,
     &                    NofR)

!!                          +-------------------+
!!                          |   P U R P O S E   |
!!                          +-------------------+
c   SEACH for REFLECTIONS 
c  ======================= 
!!
!!                 +-------------------------------------+
!!                 |   D E S C R I P T I V E   P A R T   |
!!                 +-------------------------------------+
      implicit none      
      include 'dozor_param.fi'
      include 'dozor_dimension.fi' 
      type (DETECTOR) D
      type (DATACOL)  X            
       
      integer*4 IMSIZE          ! image size
      integer*1 PSIim(imsize),KLim(imsize)      
      integer*2 image1(imsize)             
      integer*2, ALLOCATABLE :: imagePeak(:)
      REAL*8 imager, image1_to_imager  
           
      integer i,j,itt       
      integer kx,ky,ist
      integer kxt,kyt
      integer klpsi,kl      
      INTEGER*4 koor,koort  
      REAL*8 isumt 
      REAL*8 ,dimension(0:nrbin,1:npsibin)::backtabl
      REAL ,dimension(0:nrbin,1:npsibin)::sumerrd
      INTEGER ,dimension(0:nrbin,1:npsibin)::limerr                 
      INTEGER ,dimension(0:nrbin,1:nspot):: hPsi           
      INTEGER NofR 
      real ,dimension(0:nrbin)::dspot
      real dspotall,sum_spot 
      real sumt  
      real koory,koorx              
      integer l,Ispot  
      real factor,ert,bg ,correct
      real sigLev    
                             
!!               +-----------------------------------------+
!!               |   E X E C U T A B L E   S E C T I O N   |
!!               +-----------------------------------------+
!
!      determine spot minimal intensity level
!      ========================================
      sigLev=X%sigLev
      allocate(imagePeak(imsize))
        Ispot=2*ist+1
      do i=0,nrbin
         do j=1,npsibin 
            bg=backtabl(i,j)*Ispot**2
            if(bg.lt.11)then
               l=Int(bg*100+0.5)
               if(l.gt.1100)l=1100
               limerr(i,j)=X%pLim2(l)
            else
               limerr(i,j)= Int(bg+sigLev*Ispot*SQRT(backtabl(i,j)))
            endif
            itt=Int(bg+sigLev*Ispot*sumerrd(i,j)+0.5)   
            if(limerr(i,j).lt.itt)then
               if(backtabl(i,j).gt.1)then
                  correct=sumerrd(i,j)/SQRT(backtabl(i,j))
                  limerr(i,j) = 
     +               Int(bg+correct*sigLev*Ispot*sumerrd(i,j)+0.5)
               else
                  limerr(i,j)=itt
               endif   
            endif       
         enddo
      enddo
!   ===============================================     
      do koor=1,imsize
         imagePeak(koor)=0
      enddo
     
      do ky=1+ist,(D%iy-ist-1),1
         do kx=1+ist,(D%ix-ist-1),1
            koor=kx+(ky-1)*D%ix
c            imager=DBLE(image1(koor))
            imager=image1_to_imager(image1(koor),X)
            klpsi=PSIim(koor)
            kl=KLim(koor)
            if((KLim(koor).ge.0.and.PSIim(koor).gt.0)
     +         .and.imager.gt.backtabl(kl,klpsi))then     
               isumt=0.
               do i=-ist,ist
                  do j=-ist,ist
                     koort=(kx+i)+(ky+j-1)*D%ix
                     if(koor.le.imsize)then
c                        imager=DBLE(image1(koort))
                        imager=image1_to_imager(image1(koort),X)
                        isumt= isumt+ imager
                     endif
                  enddo
               enddo
               if(isumt.gt.limerr(kl,klpsi))then
                  if(isumt.gt.32000)then
                     imagePeak(koor)=32000
                  else
                     imagePeak(koor) = 
     +                 IDINT(isumt-Ispot**2*backtabl(kl,klpsi))
                 endif
              endif
            endif
         enddo
      enddo  

c  peak selection		  
      do ky=1+ist,(D%iy-ist-1),1  
         do kx=1+ist,(D%ix-ist-1),1    
            koor=kx+(ky-1)*D%ix
c    spot 	 
            imager=imagePeak(koor) 
            if(imager.gt.0.)then
               do kyt=ky-(ist+1),ky+(ist+1)
                  do kxt=kx-(ist+1),kx+(ist+1)
                     koort=kxt+(kyt-1)*D%ix
                     if(koort.ne.koor.and.koort.le.imsize)then   
                        if(ABS(DBLE(imagePeak(koort))).gt.imager)
     +                     imagePeak(koor) =-IDINT(imager)
                        if(ABS(DBLE(imagePeak(koort))).eq.imager)
     +                     imagePeak(koor) =0  
                     endif
                  enddo
               enddo 
            endif
         enddo
      enddo

      do i=0,nrbin
         X%RList(i,0)=0 
      enddo 
      NofR=0

      do ky=1+ist,(D%iy-ist-1),1
         do kx=1+ist,(D%ix-ist-1),1
            koor=kx+(ky-1)*D%ix
            if(koor.le.imsize)then
               if(KLim(koor).ge.0)then
cccccc   		 
                  if(imagePeak(koor).gt.0)then
                     kl=KLim(koor)
                     if(INT(X%RList(kl,0)).lt.nspot)then
                        X%RList(kl,0)=X%RList(kl,0)+1
                        j=INT(X%RList(kl,0))
                         X%RList(kl,j)=imagePeak(koor)
                        hPsi(kl,j)=PSIim(koor)
                        X%hklKoor(kl,j,1)=kx
                        X%hklKoor(kl,j,2)=ky
                        NofR=NofR+1  			 
                     endif
                  endif
               endif
            endif       
         enddo
      enddo

c     determine  real average size of spots and spot coordinats
      dspotall=0
      dspot=0
      itt=0
      do kl=0,nrbin
         dspot(kl)=0
         sum_spot=0
         do j=1,INT(X%RList(kl,0))
            koory=0.
            koorx=0.
            isumt=0
            sumt=0.0
            sum_spot= sum_spot+X%RList(kl,j)
            do kyt=X%hklKoor(kl,j,2)-ist-1,X%hklKoor(kl,j,2)+ist+1  
               do kxt=X%hklKoor(kl,j,1)-ist-1,X%hklKoor(kl,j,1)+ist+1
                  koor=kxt+(kyt-1)*D%ix
                  if(koor.le.imsize)then
c                     imager=DBLE(image1(koor)) 
                     imager=image1_to_imager(image1(koor),x)
                     koory= koory +imager*kyt 
                     koorx= koorx +imager*kxt
                     sumt=sumt+imager
                     isumt=isumt+ imager-backtabl(kl,hPsi(kl,j))
                  endif
               enddo
            enddo
            if(sumt.gt.0)then
               X%hklKoor(kl,j,2)= koory/sumt
               X%hklKoor(kl,j,1)= koorx/sumt
            endif
            if(isumt.lt.X%RList(kl,j))isumt=X%RList(kl,j)
            dspot(kl)=dspot(kl)+isumt
         enddo 
         if(INT(X%RList(kl,0)).ne.0)then
            dspot(kl)=dspot(kl)/sum_spot
            itt=itt+1
            dspotall=dspotall+dspot(kl)
         else
            dspot(kl)=1.
         endif
      enddo

      do kl=0,nrbin     
         do j=1,INT(X%RList(kl,0))
            X%RList(kl,j)=X%RList(kl,j)*dspot(kl)   
         enddo
      enddo
      deallocate(imagePeak)
      return         
      end
      
      
c-begin-text backg_average
!!      	
      subroutine backg_average
     & (X,LC,
     &  backtabl,backtablP,nbct,sumerrd,
     &  backpol,backpolP,backerr)
!!                          +-------------------+
!!                          |   P U R P O S E   |
!!                          +-------------------+              
c      average background distibution vs. resolution 
c     ======== ==================================================
!!
!!                 +-------------------------------------+
!!                 |   D E S C R I P T I V E   P A R T   |
!!                 +-------------------------------------+
      implicit none      
      include 'dozor_param.fi'
      include 'dozor_dimension.fi'      
      type (DATACOL)  X           
      type (LOCAL)    LC      ! local parameters 
           
      integer nbct(0:nrbin,npsibin)      
      REAL*8 , dimension(0:nrbin,1:npsibin)::backtabl,backtablP      
      real ,dimension(0:nrbin)::backpol,backpolP,backerr
      REAL sumerrd(0:nrbin,npsibin)                 
      REAL*8 sumIP,sumP2,sumbdet
      REAL*8 sumIPP,sumPP2,sumER,sumP4
      REAL polt
      INTEGER i,j,kl
      REAL delh2,h2,hmax2
      real scale_xds    
C Correction on polarisation factor and detector's background
c ===========================================================
	      	 
      scale_xds =1.0
      sumbdet=0.
      
      do i=0,nrbin
         sumIP=0.
	 sumER=0.
         sumP2=0
         sumP4=0	 
	 sumIPP=0.
	 sumPP2=0.
	 	 
               do j=1,npsibin
	 if(backtabl(i,j).gt.0.)then
	 
	 if(j.le.(npsibin/4))polt=LC%pol(i,j)
	 	 
	 if(j.gt.(npsibin/4).and.j.le.(npsibin/2))
     +polt=LC%pol(i,j-(npsibin/4))
     
	 if(j.gt.(npsibin/2).and.j.le.(3*npsibin/4))
     + polt=LC%pol(i,j-(npsibin/2))
     
	 if(j.gt.(3*npsibin/4))
     + polt=LC%pol(i,j-(3*npsibin/4))	 

            sumIP=sumIP+
     +  (backtabl(i,j)/scale_xds)*polt
            sumP2=sumP2+polt**2
	    
	   sumER=sumER+
     +((sumerrd(i,j)/scale_xds)**2/(2.*nbct(i,j)))*polt**2
            sumP4=sumP4+polt**4  
	                    endif
			    
	 if(backtablP(i,j).gt.0.)then	
	    
            sumIPP=sumIPP+
     +  ((backtablP(i,j)-backtabl(i,j))/scale_xds)*polt
            sumPP2=sumPP2+polt**2     	
	                                         endif
c                                             
                   enddo

	 if(sumP2.ne.0.0)then
         X%backpol(i)=sumIP/sumP2 
         X%backerr(i)=Sqrt(sumER/(sumP4*npsibin*4)) 
	                 else
	 X%backpol(i)=0.0 
	                 endif
	
	 if(sumPP2.ne.0.0)then
         X%backpolP(i)=sumIPP/sumPP2 	 
	                  else
	 X%backpolP(i)=0.0	 
	                  endif	
      enddo

c correction on exposure time, air absorbtion, multi-read and scale_xds
c ======================================================================

      do i=0,nrbin

          X%backpol(i)=
     & X%mrd*X%backpol(i)/(LC%absorb(i)*X%texposure)
     
          X%backerr(i)=
     & X%mrd*X%backerr(i)/(LC%absorb(i)*X%texposure)     
     &/LC%cos2tet2(i)     
          X%backpolP(i)=
     & X%mrd*X%backpolP(i)/(LC%absorb(i)*X%texposure)
     &/LC%cos2tet2(i)
  
         if(X%backpol(i).lt.0.0)then
c---------------------------------------------------------
	      
            hmax2=(X%hmin2+(i-1)*X%delh2)	 
            delh2=(hmax2-X%hmin2)/nrbin
            do j=0,nrbin
               h2=X%hmin2+j*delh2
               kl=INT((h2-X%hmin2)/X%delh2)+1
               if(kl.gt.(i-1))kl=i-1
               backpol(j)=X%backpol(kl)
               backpolP(j)=X%backpolP(kl)
	       backerr(j)=X%backerr(j)     
            enddo
	      	
            do j=0,nrbin	       
               X%backpol(j)=backpol(j)
               X%backpolP(j)=backpolP(j)
	       X%backerr(j)=backerr(j)      	       
            enddo 
	                		 
            X%hmax2=hmax2	 
            X%delh2=delh2    
            goto 10	
         endif

      enddo
 10   return
      end
      
	
c-begin-text spots_analysis
!!
      subroutine spots_analysis(
     &               X,LC,
     &               hpsi,
     &               dspot,
     &               redus,  
     &               sumerrd,
     &               NofR_real,
     &               Rfexp,
     &               Iav_best,
     &               Hlim2,
     &               sumIP)

!!                          +-------------------+
!!                          |   P U R P O S E   |
!!                          +-------------------+
c   SPOTS Intensity correction and analysis
c  ========================================= 
!!
!!                 +-------------------------------------+
!!                 |   D E S C R I P T I V E   P A R T   |
!!                 +-------------------------------------+
      implicit none  
          
      include 'dozor_param.fi'
      include 'dozor_dimension.fi' 
      
      type (DATACOL)  X            
      type (LOCAL)    LC      ! local parameters  
      
      INTEGER  hPsi(0:nrbin,nspot)          
      logical redus(nrbin)
      real dspot(0:nrbin)           
      REAL sumerrd(0:nrbin,npsibin) 
                                      
      INTEGER NofR_real !n.of finded reflection  
      REAL Rfexp, Iav_best, Hlim2 
      REAL*8 sumIP
      REAL*8 sumP2 
      REAL*8 sumER,sumP4 
      REAL*8 errort
           
      INTEGER ii         
      real n2shell,vn2shell 
      INTEGER i,j,kl,n1,jj,n2
      real polt  
      INTEGER nlimi,nlim    
      real a,b,Iav,I_max,I_min
      real dIav,Iav0,Iavt,summ,sum,Vbt
      real Jt,sJt,correct,ppdis(34)     
      real pers(nrbin+1),pers_t(nrbin)
      real h2d(nrbin) 
      INTEGER  NofR,jm
      integer  idist(34),tj(50)
      REAL Nofrt,inrmax  
      integer i5a,nn,inlim,Ispot                            
!!               +-----------------------------------------+
!!               |   E X E C U T A B L E   S E C T I O N   |
!!               +-----------------------------------------+
        Ispot=X%Ispot
	if(Ispot.eq.2)Ispot=3	
	if(Ispot.eq.4)Ispot=5
	if(Ispot.eq.6)Ispot=7
	if(Ispot.eq.8)Ispot=9
                    NofR_real=0
		     
                    i5a=INT(((1./5.0)**2-X%hmin2)/X%delh2+0.5)
	if(i5a.lt.4)i5a=4
        if(i5a.gt.nrbin)i5a=nrbin
             ii=0
	     nn=0
	     Vbt=0.
             do i=1,i5a
	   if(X%RList(i,0).ne.-1)then
	   if(X%RList(i,0).ne.0)nn=nn+1
            ii=INT(X%RList(i,0))+ii
	    Vbt=Vbt+X%vbin(i)
	                        endif
          enddo
	  
            if((ii.le.5).or.(nn.lt.2))goto 999

            n2shell = ii/Vbt

	   inlim=INT(real(ii/nn)+3*sqrt(real(ii/nn))+0.5)
           inrmax=INT(X%RList(1,0))
           
               nlimi=1
		  do i=2,i5a
      if(inrmax.lt.INT(X%RList(i,0))/X%vbin(i))then
         if(X%RList(i,0).le.inlim)then
         inrmax=INT(X%RList(i,0))/X%vbin(i)
         nlimi=i
      endif
                                    endif
                 enddo 

       do i=1,nrbin !   calculate density
          h2d(i)=X%hmin2+(i-0.5)*X%delh2
	  pers(i)=-1
	  if(X%RList(i,0).ne.-1)
     +       pers(i)=X%RList(i,0)/X%vbin(i)	     		    
       enddo

c -----------------------------------------------

cc preliminary reduction of outliers 
       do i=1,nrbin	 
          if(pers(i)/inrmax.gt.1.5)then
       	     pers(i)=-1
	     X%RList(i,0)=-1
	     X%backpolP(i)=0
	                         endif
        enddo	
ccccccccccccccccccccccccccccccccccccccccccc
c first estimate nlimi 
        nlim=nrbin
	j=0
       do i=1,nrbin
         if(pers(i).ne.-1)then
	    j=j+1
	    pers_t(j)=pers(i)	
            tj(j)=i
	                  endif
	enddo
	
	jm=j
	
	do i=5,jm-2
       if((pers_t(i-2)+pers_t(i-1)+pers_t(i)
     &+pers_t(i+1)+pers_t(i+2)).le.(3/X%vbin(tj(i))))then       
       nlim=tj(i-2)
              exit
              endif
           enddo
           
             ii=0
	     nn=0
             do i=1,nlim
	   if(X%RList(i,0).ne.-1)then
	   if(X%RList(i,0).ne.0)nn=nn+1
            ii=INT(X%RList(i,0))+ii
	                        endif
          enddo
	  
            if((ii.le.5).or.(nn.lt.2))goto 999		   
ccccccccccccccccccccccccccccccccccccccccccc
c liniar aproximation
                       do ii=1,3
         sumIP=0.
	 sumER=0.
         sumP2=0
         sumP4=0
	 n1=0
       do i=1,nlim      
         if( pers(i).ge.0)then
	 n1=n1+1
	 sumIP= sumIP+h2d(i)* pers(i)
	 sumER=sumER+h2d(i)
	 sump2=sump2+h2d(i)**2
	 sump4=sump4+pers(i)
	                 endif 		       
       enddo    
           if(n1.le.1)goto 999

       a=(sumIP-sumER*sump4/n1)/(sump2-sumER**2/n1)       
       b=(sump4-a*sumer)/n1     
         errort=0
	 n1=0
	
       do i=1,nlim
         if( (a*h2d(i)+b).gt.0.and.pers(i).ge.0)then	
          n1=n1+1
	  errort=errort+(a*h2d(i)+b-pers(i))**2
                                           endif
	enddo
	 if(n1.eq.0)goto 999
	errort=X%sigLev*SQRT(errort)/n1
				   		 
       do i=1,nlim      
        if( (a*h2d(i)+b).gt.0.and.pers(i).ne.0)then	
	 if(((pers(i)-(a*h2d(i)+b)).gt.errort).and.
     +(pers(i)-(a*h2d(i)+b)).gt.2*(a*h2d(i)+b))then
          
             X%RList(i,0)=-1
             pers(i)=-1
	      X%backpolP(i)=0
	                                        endif
                                           endif			   					  		 
       enddo
                        enddo
			
         goto 334			
cttttttttttttttttttttttttttttttttttttttttt
         open(12,file='spot_den.mtv',status="unknown")            
c Write file for Plotmtv
c ======================
 510          FORMAT(a)
      WRITE(UNIT=12,fmt=510)"$ DATA=CURVE2D"
      WRITE(UNIT=12,fmt=510)"% toplabel  = 'Spot density'"

      WRITE(UNIT=12,fmt=510)"%xmin = 0"
      WRITE(UNIT=12,fmt=510)"% xlabel = '1/ Resolution**2'"
      WRITE(UNIT=12,fmt=510)"% ylabel = 'density'"

      WRITE(UNIT=12,fmt=510)"# Curve 1"
      WRITE(UNIT=12,fmt=510)"% linetype   = 11"  
      WRITE(UNIT=12,fmt=510)"% linewidth  = 3"   
      WRITE(UNIT=12,fmt=510)"% linecolor  = 3"   
      WRITE(UNIT=12,fmt=510)"% linelabel  = 'density theor'"

                             do i=1,nlim

      WRITE(UNIT=12,fmt='(f5.3,2x,f10.5)')
     + h2d(i),(a*h2d(i)+b)

                             enddo
			     
      WRITE(UNIT=12,fmt=510)"# Curve 1"
      WRITE(UNIT=12,fmt=510)"% linetype   = 0"  
      WRITE(UNIT=12,fmt=510)"% markertype = 2"   
      WRITE(UNIT=12,fmt=510)"% markercolor  = 4"   
      WRITE(UNIT=12,fmt=510)"% markersize  = 1"
                             do i=1,nlim
       if(pers(i).ge.0)
     +      WRITE(UNIT=12,fmt='(f5.3,2x,f10.5)')
     + h2d(i),pers(i)

                             enddo			     
			     
            close(unit=12)       
334     continue       
cttttttttttttttttttttttttttttttttttttttttttttttttt		
	   n1=INT(((0.0002-b)/a -X%hmin2)/X%delh2 +0.5)  
	if(nlim.gt.n1.and.n1.gt.0)nlim=n1

             NofR_real=0 
	     nlimi=nlim
                if(nlim.gt.nrbin)nlim=nrbin
      do i=1,nlim 
        If(X%RList(i,0).gt.0)
     +  NofR_real= NofR_real+INT(X%RList(i,0)) 
      enddo
                   if(NofR_real.eq.0)goto 999
c -----------------------------------------------
        sumIP=0.
	 n1=0
	 n2=0					     	        
	 do i=nlim+1,nrbin
	    X%RList(i,0)=-1 
	 enddo
		    

c pol. and absorb correct
                             do i=0,nrbin
	                   do j=1,INT(X%RList(i,0))
	          kl=hPsi(i,j)
	    if(kl.gt.npsibin)kl=npsibin
	 if(kl.le.(npsibin/4))
     &      polt=LC%pol(i,kl)
	 if(kl.gt.(npsibin/4).and.kl.le.(npsibin/2))
     &      polt=LC%pol(i,kl-(npsibin/4))	             
	 if(kl.gt.(npsibin/2).and.kl.le.(3*npsibin/4))
     &      polt=LC%pol(i,kl-(npsibin/2))                
	 if(kl.gt.(3*npsibin/4))
     &      polt=LC%pol(i,kl-(3*npsibin/4))
     		  	
	     polt=polt*LC%absorb(i)    
	  if(polt.ne.0) 
     &	  X%RList(i,j)=X%RList(i,j)/polt
	                 enddo	 
	                  enddo
c
c   ilim estimate 
c------------------------------------------------      
       do i=1,nrbin 
        
           X%Ilimit(i)=0.
	  
               do j=1,npsibin/4 
	     kl=j
	     polt=LC%pol(i,kl)*LC%absorb(i)
	     if(polt.ne.0)
     + X%Ilimit(i)=X%Ilimit(i)+X%sigLev*Ispot*sumerrd(i,j)/
     +  polt
               enddo
	      
               do j=npsibin/4+1,npsibin/2
	     kl=j-(npsibin/4)
	     polt=LC%pol(i,kl)*LC%absorb(i)
             if(polt.ne.0)     
     + X%Ilimit(i)=X%Ilimit(i)+X%sigLev*Ispot*sumerrd(i,j)/
     +  polt
               enddo
	      
               do j=npsibin/2+1,3*npsibin/4
	     kl=j-(npsibin/2)
	     polt=LC%pol(i,kl)*LC%absorb(i)
             if(polt.ne.0) 	     
     +  X%Ilimit(i)=X%Ilimit(i)+X%sigLev*Ispot*sumerrd(i,j)/
     +   polt
              enddo   
	      
             do j=3*npsibin/4+1,npsibin
	     kl=j-(3*npsibin/4)
	     polt=LC%pol(i,kl)*LC%absorb(i)
             if(polt.ne.0) 	     
     +  X%Ilimit(i)=X%Ilimit(i)+X%sigLev*Ispot*sumerrd(i,j)/
     +    polt
              enddo   	      	     
	   X%Ilimit(i)=dspot(i)*X%Ilimit(i)/npsibin 
       enddo
c -----------------------------------------------

c ---------------------------------------

	  I_max=0
	  I_min=1e6  
	  Iav=0 
	  NofR=0 
       do i=1,nrbin 
        do j=1,INT(X%RList(i,0))
	 if(I_max.lt.X%RList(i,j))I_max=X%RList(i,j)
	 if(I_min.gt.X%RList(i,j))I_min=X%RList(i,j)	 
	 NofR=NofR+1
	 Iav=Iav+X%RList(i,j)
	enddo
       enddo
       	if(NofR.ne.0)Iav=Iav/NofR

          NofRt=NofR*0.03   !! 3% from total number of spot per interval, 33 intervals           
          dIav=0.05*(Iav-I_min)
          Iav0=Iav
            do ii=1,2
ccccccccc+++++++++++++++++++++++++++++++++++++++++++++++++++	    
	      if(ii.eq.2)then
                 dIav=0.01*Iav_best
                 Iav0=Iav_best
		         endif
			 	      
                   do j=-19,19
	 Iavt=dIav*j+Iav0
        correct=exp(-I_min/Iavt)
           Jt=I_min
	   ppdis(1)=Jt

       do i=1,33
        SJt=-Iavt*
     &     ALOG(Exp(-Jt/Iavt)-0.03*correct)
        ppdis(i+1)=SJt
        Jt=SJt
       enddo	 
	 
         do i=1,34 
	 idist(i)=0 
	 enddo	 
	 
       do i=1,nrbin 
         do jj=1,INT(X%RList(i,0))
	 Jt=X%RList(i,jj)
	    do kl=1,33
	    if(Jt.ge.ppdis(kl).and.Jt.lt.ppdis(kl+1))then
	       idist(kl)=idist(kl)+1
	       exit
	       endif
	    enddo
	    if(Jt.ge.ppdis(34))idist(34)=idist(34)+1	       
	 enddo
       enddo  
       
         sum=0.

         do i=1,33  
       sum=sum+	             
     &(idist(i)-NofRt)**2
         enddo
	 
       sum=sum+	             
     &(idist(34)-correct*NofRt/3)**2	 
       
	 if(j.eq.-19)summ=sum
	 if(j.eq.-19)Iav_best=Iavt
	 	 
	 if(summ.gt.sum)then
	   Iav_best=Iavt
	   summ=sum
	               endif
		       	      	 
                  enddo		
ccccccccc+++++++++++++++++++++++++++++++++++++++++++++++++++
                 enddo
ccccccccc+++++++++++++++++++++++++++++++++++++++++++++++++++
        sum=0.
        sumIP=0.     

	 Iavt=Iav_best
        correct=exp(-I_min/Iavt)
           Jt=I_min
	   ppdis(1)=Jt

       do i=1,33
       SJt=-Iavt*
     &ALOG(Exp(-Jt/Iavt)-0.03*correct)
       ppdis(i+1)=SJt
       Jt=SJt
       enddo	 

         do i=1,34 
	 idist(i)=0 
	 enddo	 
	
       do i=1,nrbin 
         do j=1,INT(X%RList(i,0))
	 Jt=X%RList(i,j)
	    do kl=1,33
	    if(Jt.ge.ppdis(kl).and.Jt.lt.ppdis(kl+1))then
	       idist(kl)=idist(kl)+1
	       exit
	       endif
	    enddo
	    if(Jt.ge.ppdis(34))idist(34)=idist(34)+1	       
	 enddo
       enddo  
     	               	        
         do i=1,33 
      sumIP=idist(i)
        sum=sum+
     &abs(sumip-0.03*correct*NofR)	         
 
	 enddo 
        sum=sum+abs
     &(sumip-0.01*correct*NofR)
	Rfexp=sum /(2*NofR)
		 
c corrections ====================================				 
	Hlim2=X%hmin2+(nlimi-1)*X%delh2
	
	     sumIP=0.
	     sum=0.
	do i=1,nlim-1	
	if(redus(i))then
	if(X%RList(i,0).gt.0)then
	sumIP=sumIP+X%backpolP(i)*X%vbin(i)
	sum=sum+X%vbin(i)
	                     endif
	            endif
	enddo
	
	      if(sumIP.eq.0.)goto 999
c	print*,nlim,Iav_best,1000*sumIP/sum
c     +	,sum*Iav_best/sumIP	      
	sumIP=sum*Iav_best/sumIP
c	sumIP=sum*Iav/sumIP	
crrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr
	I_max=5*Iav_best
	ppdis(1)=I_min
       do i=1,33
       SJt=i*(I_max-I_min)/33+I_min
       ppdis(i+1)=SJt
       enddo	
         do i=1,34 
	 idist(i)=0 
	 enddo	 
	 
       do i=1,nrbin 
         do j=1,INT(X%RList(i,0))
	 Jt=X%RList(i,j)
	    do kl=1,33
	    if(Jt.ge.ppdis(kl).and.Jt.lt.ppdis(kl+1))then
	       idist(kl)=idist(kl)+1
	       exit
	       endif
	    enddo
	    if(Jt.ge.ppdis(34))idist(34)=idist(34)+1	       
	 enddo
       enddo  

       NofR_real=NofR_real*exp(I_min/Iav_best)

       j=0      
       do i=1,33
       j=j+idist(i)     
       enddo  
         
crrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr        	
		       
		       return
		       
999      continue		       		       
	 NofR_real=0		              
         Rfexp=0
	 Iav_best=0
	 Hlim2=(1./nrbin)**2		       
	 sumIP=1000.		       

	
      return         
      end	



	  
	      
c-begin-text WILSON_single
      
      SUBROUTINE WILSON_single(D,X,Xp,LC)

!!                          +-------------------+
!!                          |   P U R P O S E   |
!!                          +-------------------+
c   program for determination scale and B-factors  
c   using standart Wilson curve 
c

!!
!!                 +-------------------------------------+
!!                 |   D E S C R I P T I V E   P A R T   |
!!                 +-------------------------------------+

      IMPLICIT NONE 
      include 'dozor_param.fi' 
      include 'dozor_dimension.fi'  
             
      type (DETECTOR)   D       ! DETECTOR parameters
      type (CRYSTAL)    C       ! CRYSTAL parameters
      type (DATACOL)    X       ! D.C.    parameters
      type (DATACOL_PICKLE) Xp  ! ingle frame in this routine
      type (LOCAL)      LC      ! local parameters      
      
      INTEGER irbin
      PARAMETER( irbin=700)
      REAL idealdis(irbin),h2disw(irbin),delh2
      include 'wilsid.fi' 
      
      INTEGER*4 nrefl
      PARAMETER(nrefl=nrbin)

      INTEGER*4 nrefr,nrefm,i,nsub

cgggggggggggggggggggggggggggggggggggggggg

c      REAL*8, dimension(400):: awvect	!nrefl*8)
c      real*8 sb,hbg(6),errt
cgggggggggggggggggggggggggggggggggggggggg
c      integer sing,flaws,kvt,awsfit
      integer awsfit
      
      REAL*8, dimension(nrbin) :: f2,sigf2,h2	!all nrefl
      REAL*8, dimension(nrbin) :: f2t,sigf2t,h2t	!all nrefl 

      REAL, dimension(0:nrbin) :: vbins         
      REAL h2max,h2min 

c matrix of coefficients for B-factor and max.number of coeff.
c ===================================
      REAL hb2(6,nrbin)	!(6,nrefl)

c   aver.intens. distribution parameters
c   =====================================
      REAL intdis(irbin),sigdis(irbin),intdist(irbin),sigdist(irbin)
      REAL intdistc(irbin)      
      REAL intdisc,sigdisc      
      REAL h2mint,h2maxt,h2maxnl,h2minnl
      INTEGER  nofdis(irbin),nofdist(irbin),nbinmin,nbinmax,kl  
      INTEGER nofdisc,isumT
c Local variables
c ==================

      INTEGER j,flgl,flfail
      INTEGER flover(irbin)
      REAL Iidealt
      integer first,iios,nphninm,nbinmaxt,nall 
      REAL*8 errmax,errmin

      integer nbinmaxorg
      integer nb
      real*8 ev(3)
      real dum,corr,dsum_min,rfact 
      real ccm,cc,bf,bft,dsum
      real sum1,sum2,sum3,intdistS,intdisS,intial
      integer ks,n1,n2,kt
      real ht1,ht2
      real vbin(nrbin)           
      integer      l3,k     
           
!!               +-----------------------------------------+
!!               |   E X E C U T A B L E   S E C T I O N   |
!!               +-----------------------------------------+

c      vbins(0)=0
c      do i=1,nrbin
c         ht1=Sqrt(X%hmin2+(i-1)*X%delh2)
c         ht2=Sqrt(X%hmin2+i*X%delh2)
c         vbin(i)=ht2**3-ht1**3 
c         vbins(i)=vbins(i-1)+vbin(i)
c      enddo   
        
      do i=1,700       
         C%idealdist(i)= idealdis(i)/100.
      enddo 

      Xp%table_suc=.true. 
      
      do kt=1,3
         C%kv=1
         delh2=(h2disw(irbin)-h2disw(1))/(irbin-1)
         h2min=h2disw(1)-delh2/2.
         h2max=h2disw(irbin)+delh2/2
      

c max_ and min_ resolution limits determination
c ----------------------------------------------
         h2maxt=0.0
         h2mint=h2disw(irbin)
         nrefr=0

         do i=1,nrbin
            h2t(i)=(X%hmin2+(i-1)*X%delh2)+X%delh2/2
            f2t(i)=X%backpolP(i)
            sigf2t(i)=X%backerr(i)

c          selection ice
            if(kt.ne.1)then
               kl=INT((h2t(i)-h2min)/delh2)+1
               C%hbg(1) = h2t(i) 
               CALL awsappl (IABS(C%kv),C%scvt,C%cov,C%hbg,C%sb)
               intial=C%idealdist(kl)*C%sb
               if((f2t(i)-intial)/intial.ge.2.)f2t(i)=0
            endif
	 		 		          
           if(h2t(i).ge.h2min.and.f2t(i).gt.0)THEN
            if(h2maxt.le.h2t(i))h2maxt=h2t(i)
            if(h2mint.ge.h2t(i))h2mint=h2t(i)
            nrefr=nrefr+1
            h2(nrefr)=h2t(i)
            f2(nrefr)=f2t(i)
            sigf2(nrefr)=sigf2t(i)
            hb2(1,nrefr)= h2t(i)
         endif
	 enddo
      
      if(h2maxt.gt.X%hmax2)h2maxt=X%hmax2
        
c ------------------------------------------------------------

c  Average intensity vs. resolution
c  =================================

c  selection of n.of bins
c =========================
      if(h2maxt.le.h2max)then
         nbinmax=INT((h2maxt-h2min)/delh2)
         h2maxt=nbinmax*delh2+h2min
      else
         nbinmax=irbin
         h2maxt= h2max
      endif       
      
      if(h2mint.le.(h2min+delh2))then
         nbinmin=1
      else
         nbinmin=Int((h2mint-h2min)/delh2)+1                 
      endif
c     
c     all =  zero
c-----------------------------------------------------------
      do i=nbinmin,nbinmax
         intdis(i)=0.
         sigdis(i)=0.
         nofdis(i)=0
         flover(i)=0
      enddo
      
c------------------------------------------------------------------
      h2maxnl=h2min
      h2minnl=h2disw(irbin)
      
c ------------------------------------------------------------

      do i=1,nrefr

         if((h2(i).ge.h2mint).and.(h2(i).le.h2maxt))then 
            kl=INT((h2(i)-h2min)/delh2)+1

            intdis(kl)= intdis(kl)+f2(i)
            sigdis(kl)= sigdis(kl)+abs(sigf2(i))
            nofdis(kl)=nofdis(kl)+1
       endif
      enddo
c average value
c===============
      do i=nbinmin,nbinmax
      
         if((nofdis(i).ne.0).and.(flover(i).eq.0))then
            intdis(i)=intdis(i)/nofdis(i)
            sigdis(i)=sigdis(i)/nofdis(i)

            if(intdis(i).le.0.0.and.nofdis(i).gt.20)flover(i)=-1
         endif
      enddo

c statistical check -selection of the max resolution as the shell where
c intensity/sigma > 2.0

      nbinmaxt=nbinmin
      i=nbinmin
      
	isumT=0     
      do while(i.le.nbinmax)
 
         nofdisc=nofdis(i)
	 intdisc=intdis(i)*nofdis(i)
	 sigdisc=sigdis(i)*nofdis(i)

	    i=i+1  
	  if( nofdis(i).ne.0)then  
         nofdisc=nofdisc+nofdis(i)
	 intdisc=intdisc+intdis(i)*nofdis(i)	   	    	 
	 sigdisc=sigdisc+sigdis(i)*nofdis(i)

         if(intdisc/sigdisc.lt.0.5)then

	 isumT=isumT+1
	                           else
	 isumT=0
	                          endif				   

          if(isumT.gt.3)nbinmaxt=i-2	  
	  if(isumT.gt.3)exit

           if(intdisc/sigdisc.lt.0.)exit	   
	                     endif
           nbinmaxt=i
	   i=i+1	   	 	    
      enddo
      
      nbinmaxorg=nbinmax
      nbinmax=nbinmaxt
      h2maxt=nbinmax*delh2+h2min

      nall=0
      do i=nbinmin,nbinmax   
         if((nofdis(i).ne.0).and.(flover(i).eq.0))
     +        nall=nall+1
      enddo

      flfail=0

 777  continue

      C%kvt=IABS(C%kv)+2
      nsub=0
      
      do i=1,nrefr
            if((h2(i).ge.h2mint).and.(h2(i).le.h2maxt))then
            kl=INT((h2(i)-h2min)/delh2)+1

c .............................................................
c preparetion for Gleb subroutine
c ================================
        Iidealt=C%idealdist(kl)+
     +  (C%idealdist(kl+1)-C%idealdist(kl))*(h2(i)-h2disw(kl))/delh2

            nsub=nsub+1
    
            if(h2maxnl.le.h2(i))h2maxnl=h2(i)
            if(h2minnl.ge.h2(i))h2minnl=h2(i)
            
            C%awvect((nsub-1)*C%kvt+1) = f2(i)   
            C%awvect((nsub-1)*C%kvt+2) = Iidealt
            C%awvect((nsub-1)*C%kvt+3) = h2(i)
    
                     endif
c		     endif
c ..............................................................
           
        enddo

c-------------------------------------------------------------

c calculation of wilson coefficients
c ====================================

        C%flaws =
     +       awsfit(nsub,C%awvect,IABS(C%kv)+1,C%scvt,C%cov,C%sing)


c ---------------------------------

      if((C%sing.ne.0.or.C%flaws.ne.0) )
     +Xp%table_suc=.false.  
      if(Xp%table_suc.eqv..false. ) then
      Xp%table_sc=0
      Xp%table_b=0
      Xp%table_resol= 0  
                                endif            
      if(Xp%table_suc.eqv..false. )goto 990
      
         if(kt.eq.3)then
       call evb(C%kv,C%scvt,dum,ev)
c       if(dum.le.-7.5)Xp%table_suc=.false.               	 				
           endif 

      if(Xp%table_suc.eqv..false. )goto 990	   

          enddo   !end kt 
 
c Preparetion for intesity plot
c ==============================
        do i=nbinmin,nbinmaxorg
           intdist(i)=0.
           sigdist(i)=0.
           nofdist(i)=0
        enddo

        do i=1,nrefr

       if((h2(i).ge.h2disw(1)).and.(h2(i).le.h2disw(irbin)))then
              kl=INT((h2(i)-h2min)/delh2)+1
              if(sigf2(i).gt.0.0)then
                 
                    C%hbg(1) = h2(i)
                 
                 CALL awsappl (IABS(C%kv),C%scvt,C%cov,C%hbg,C%sb)
                 intdist(kl)= intdist(kl)+C%sb
                 nofdist(kl)=nofdist(kl)+1
              endif    
           endif
        enddo  
	
        do i=nbinmin,nbinmaxorg
	 
           if(nofdist(i).ne.0)then
              intdist(i)=C%idealdist(i)*intdist(i)/nofdist(i)
              sigdist(i)=sigdist(i)/nofdist(i)
	      
           endif
        enddo
          
	  if(X%graph)then
                   j=0
             do i=nbinmin,nbinmax
      if((nofdis(i).ne.0).and.
     &  (flover(i).eq.0).and.(intdist(i).gt.0.))then
	        j=j+1
	     X%Wil(1,j)=h2disw(i)
	     X%Wil(2,j)=intdist(i)
	     X%Wil(3,j)=intdis(i)
	                                       endif
	     enddo
	     X%Wil(1,701)=j	
	              endif  
	  
c=======================
        call evb(C%kv,C%scvt,dum,ev)
     
      Xp%table_suc=.true.
      Xp%table_sc=C%scvt(1)
      Xp%table_b=2.*dum
      Xp%table_resol= sqrt(1/(h2maxt))

	    intdistS=0
	    intdisS=0
	    rfact=0
	    ks=0   
       if(nbinmaxorg.lt.(nbinmin+2))then
       	corr=0.
	rfact=1
	                             else    
                 do i=nbinmin,nbinmaxorg
		  if(intdist(i).gt.0)then		 
		 ks=ks+1
		   intdistc(i)=intdist(i)
		   intdistS=intdistS+intdistc(i)
		   intdisS=intdisS+intdis(i)
		   rfact=rfact+abs(intdis(i)-intdistc(i))
		                     endif
		 enddo 
		   intdistS=intdistS/ks
		   intdisS=intdisS/ks	
		sum1=0.
		sum2=0.	
		sum3=0.	
                 do i=nbinmin,nbinmaxorg
		  if(intdist(i).gt.0)then		 		
	sum1=sum1+(intdistc(i)-intdistS)*(intdis(i)-intdisS)
	sum2=sum2+(intdistc(i)-intdistS)**2 				   	   	    
	sum3=sum3+(intdis(i)-intdisS)**2 
	                            endif	
	         enddo
		 if(sum2*sum3.gt.0)then
	corr=sum1/Sqrt(sum2*sum3)
	if(corr.lt.0)corr=0
	                          else
	corr=0.0
	                       endif
	   if(((intdistS+intdisS)*ks).gt.0)then		       				  
	rfact=rfact/((intdistS+intdisS)*ks)
	                                  else
         rfact=0.
	 endif	
	                                  endif	
					  			  
        if(Xp%table_b.le.0.and.(Xp%table_resol.gt.2.9)
     + .and.(Xp%NofR.le.25))then
           corr=0.	 
	   Xp%table_suc=.false. 
	endif	
				  
        Xp%table_corr=corr
        Xp%table_rfact=rfact
     
c           print*,ii,Xp%table_b(ii)
        sum1=0.
	sum2=0.
         do i=1,nrbin 
        h2t(i)=(X%hmin2+(i-1)*X%delh2)+X%delh2/2
	if(h2t(i).gt.h2maxt)exit
              kl=INT((h2t(i)-h2min)/delh2)+1
                     C%hbg(1) = h2t(i) 
                 CALL awsappl (IABS(C%kv),C%scvt,C%cov,C%hbg,C%sb)
           intial=C%idealdist(kl)*C%sb		
        sum2=sum2+X%vbina(i)		     
        sum1=sum1+X%vbina(i)*intial
          enddo
	  
	  Xp%table_intsum=sum1/sum2

990     continue
9999   continue	
	
        return
          END
cvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
