

c-begin-text  ctag
c
c selects n-th ascii substring tag from input character string c 
c return value :  0  - failure 
c                 1  - succsess
c                -1  - tag truncated
c
c note: len(tag) finction used, i.e. the fuction will only work safely
c if tag size is declared in a routine colling ctag 
c
      integer function ctag(n,c,tag)
      implicit none
      character*(*) c
      integer n
      character*(*) tag
c
      integer lc,lt
      integer i,j
      integer len
      integer ichar
      integer*1 b
      integer*1 intag
      integer ntag
      integer tagl
      integer status
      integer lnblnk
c
      lc = lnblnk(c)+1
      if(n.le.0.or.n.gt.(lc+1)/2)then
         ctag = 0
         return
      endif
      lt = len(tag)
      intag = 0
      ntag = 0
      tagl = 0
      do i=1,lc
         b=ichar(c(i:i))
c         print*,b,c(i:i)
         if(b.ge.33.and.b.lt.126)then
            if (intag.ne.1)ntag=ntag+1
            intag = 1
            tagl=tagl+1
            if(tagl.gt.lt)then
               ctag = -1
               if(ntag.eq.n) return
            else
               tag(tagl:tagl)=c(i:i)
            endif
         else
            ctag = 1
            if(ntag.eq.n) then
               do j=tagl+1,lt
                  tag(j:j)=char(32)
               enddo
               return
            endif
            tagl = 0
            intag = 0
         endif
      enddo
      ctag = 0
      return
      end

      subroutine dozor_tellhelp()
c     
      implicit none
      character*1024 arg
      logical fail,isthere
      CHARACTER string*1024   

      call lscancl("--help",isthere,fail)    
      if(isthere)then
      write(0,'(a,/,a,a,/,a,/,a)')
     +' ', 
     +'Rapid scoring of X-ray diffraction patterns ',
     +'from macromolecular crystal',          
     +'       Commmand Line Interface ',
     +'       ========================'
      write(0,'(a)')     	
     +'SYNOPSIS', 
     +' dozor [OPTIONS] dozor.dat' 

      write(0,'(/,a)')      
     +'OPTIONS'
      write(0,'(a)')       
     +'--help  gives help message'
     
      write(0,'(a/a)')      
     +'-bin binning_factor',
     +'         Determine detector binning, default 1'     
     
      write(0,'(a,a,a)')      
     +'-b      writes ',
     +'background intensity vs.resolution and image number',
     +' in the file: background_res.dat ',
     +'         and generates mtv_plot: dozor_background.mtv' 
                     
      write(0,'(a,a)')      
     +'-p      writes the list of spots coordinates',
     +' in the files: #####.spot '
     
      write(0,'(a,a,/,a)')        
     +'-g      mtv_plots of B-factors and 2-D spots are generated',
     +'     and stored in files:',
     +'          dozor_wilson_#####.mtv and dozor_spot_#####.mtv' 
     
      write(0,'(a)')        
     +'-w      generates file: dozor_average.dat' 
          
      write(0,'(a/,a)')        
     +'-wg     generates file and mtv_plot:',
     +'          dozor_average.dat   dozor_average.mtv '      
      
      write(0,'(a,/,a,a)')        
     +'-rd     generates Radiation Damage mtv_plot: dozor_rd.mtv ',
     +'         and estimates haft-dose Time ' 
     
      write(0,'(a)')        
     +'-s      generates table: dozor_sum_int.dat ' 
     
          
      write(0,'(a)')        
     +'-pall    print all estimates ' 
                  
         call exit(0)
      endif 
      return
      end        

c-begin-text place
!
      subroutine place(i,x,y,m,p)
      integer*4 i,x,y,m,p
      integer int

      goto(1,2,3,4,5,6,7,8)m
 1    p=i
           goto 10
 2    int=(i-1)/y
      p=(i-int-1)*x+int+1
            goto 10
 3    int=(i-1)/x
      p=y-(i-2*int*x)+1
            goto 10
 4    int=(i-1)/y
      p=(i-int*y-1)*x+int+1
            goto 10
 5    int=(i-1)/y
      p=(x+int*y-i)*x-int+y
            goto 10
 6    p=(x-1)*x+y-i+1
            goto 10
 7    int=(i-1)/x
      p=(x-1)*x+i-2*int*x
            goto 10
 8    int=(i-1)/y
      p=(x-i+int*y-i)*x+int+1
 10   continue
      return
      end

c-begin-texti2swap
  
      subroutine i2swap(b)
      integer*1 b(2)
      integer*1 buf(2)
      buf(1)=b(2)
      buf(2)=b(1)
      do i=1,2
         b(i)=buf(i)
      enddo
      return
      end


      integer*4 function ushort2long(x)
      integer*2 x
      if(x.lt.0)then
         ushort2long=65536+x
      else
         ushort2long=x
      endif
      return
      end
      
cc factorial
      real function factor(n)
      implicit none
      integer*4 i
      real sum
      integer, intent(IN) :: n            

       sum=1
       do i=1,n
          sum=sum*i
       enddo
       factor=sum
       end function factor

