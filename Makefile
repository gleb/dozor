prefix	=	/opt/embl-hh
build_date	:=	$(shell date +%Y%m%d)
os	:= $(shell uname -s)
machine	:= $(shell uname -m)
version	= 1.0.0
#FC 	=  gfortran -fopenmp -pg
FC     =  gfortran  -fopenmp
CC	=  g++  -I./ -g
FCFLAGS = -finit-local-zero -O3 -w -ldl 
#FCFLAGS = -g3 -finit-local-zero -w -Wall -Wextra -fsanitize=address -fno-omit-frame-pointer

LDFLAGS = 

DOZOR = dozor

INCL = -I/usr/local/include/cbflib -I/usr/include/
LIBS = -lz -lcbf -lstdc++ -lrt
#-lasan

##
##
##

OBJSdozor   =  generic_data_plugin.o dozor_main.o dozor_submain.o dozor_rw_lib.o dozor_auxiliary_lib.o anis_gleb_all.o hkl_direct.o scancl.o

$(DOZOR) : $(OBJSdozor)
	$(FC) $(FCFLAGS) -o $(DOZOR) $(OBJSdozor) $(LDFLAGS) 


anis_gleb_all.o: anis_gleb_all.f
	$(FC) $(FCFLAGS) -c anis_gleb_all.f
#	$(FC) -c -w -O3 anis_gleb_all.f

generic_data_plugin.o:
	$(FC) $(FCFLAGS) -c generic_data_plugin.f90

dozor_main.o: dozor_main.f90 dozor_param.fi dozor_dimension.fi generic_data_plugin.f90
	$(FC) $(FCFLAGS) -c dozor_main.f90

cozor.o: cozor.c
	$(CC) $(INCL) -c cozor.c dozor_pass_defs.h

dozor_submain.o: dozor_submain.f dozor_param.fi dozor_dimension.fi
	$(FC) $(FCFLAGS) -c dozor_submain.f

dozor_rw_lib.o: dozor_rw_lib.f dozor_param.fi dozor_dimension.fi
	$(FC) $(FCFLAGS) -c dozor_rw_lib.f

hkl_direct.o: hkl_direct.f dozor_param.fi dozor_dimension.fi
	$(FC) $(FCFLAGS) -c hkl_direct.f

#dozor_ri2xds.o: dozor_ri2xds.f dozor_param.fi dozor_dimension.fi 
#	$(FC) $(FCFLAGS) -c dozor_ri2xds.f

dozor_auxillary_lib.o: dozor_auxillary_lib.f dozor_param.fi dozor_dimension.fi 
	$(FC) $(FCFLAGS) -c dozor_auxillary_lib.f

ar: dozor_submain.o dozor_auxiliary_lib.o anis_gleb_all.o hkl_direct.o scancl.o
	ar rvcs libdozor.a dozor_submain.o dozor_auxiliary_lib.o anis_gleb_all.o hkl_direct.o scancl.o 

#dozor_ri2xds.o

.f90.o:
	$(FC) $(FCFLAGS) -c $<

.f.o: 
	$(FC) $(FCFLAGS) -c $<

.c.o:
	$(CC) $(INCL) -c $<

all:  ${DOZOR}

clean:
	rm -f	*.mod *.o *.a *.so

archive: 
	tar cvfz dozor_${build_date}.tgz *.f90 *.f *.fi Makefile

install: 
	install libdozor.a $(prefix)/lib

.PHONY: 
	install

