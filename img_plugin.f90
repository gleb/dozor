! build with gfortran -fpic -shared img_plugin.f90 -openmp -o img_plugin.so
MODULE plugin_test_mod
   implicit none
   CHARACTER :: template*1024
   integer ::  from, to, HEADER_BYTES
   character*4 fmt
END MODULE

SUBROUTINE plugin_open(filename, info_array, error_flag) bind(C)
   USE ISO_C_BINDING
   USE plugin_test_mod
   implicit none
   integer(c_int) :: error_flag
   character(kind=c_char) :: filename(*)
   integer(c_int), dimension(1024) :: info_array
   integer i
   do i=1,1024 
      if ( filename(i) .ne. C_NULL_CHAR ) then
         template(i:i) = filename(i)
      else
         exit
      endif
   enddo
   print*,template
   from = index(template,'?')
   to = index(template,'?', back=.true.)
   fmt = "I . "
   write(fmt(2:2),'(i1)')to - from + 1
   write(fmt(4:4),'(i1)')to - from + 1
   error_flag = 0
   info_array = 0
END SUBROUTINE plugin_open

SUBROUTINE plugin_get_header(nx, ny, nbyte, qx, qy, number_of_frames, info_array, error_flag) bind(C)
   USE ISO_C_BINDING
   USE plugin_test_mod
   implicit none
   integer(c_int)                  :: nx, ny, nbyte, number_of_frames
   real(c_float)                   :: qx, qy
   integer(c_int)                  :: error_flag
   integer(c_int), dimension(1024) :: info_array

   integer i
   integer BitmapSize
   character*1024 str
   integer fd, omp_get_thread_num
   fd = 11+omp_get_thread_num()
   str=template
   write(str(from:to),"("//fmt//")") 1
   open(fd, file = str)
   HEADER_BYTES = 0
   nx=0
   ny=0
   qx=0
   qy=0
   do i = 1, 64
      read(fd,'(a1024)')str
      if (index(str,"HEADER_BYTES=") == 1)then
          read(str(len("HEADER_BYTES=")+1:),*) HEADER_BYTES
      elseif (index(str,"BitmapSize=") == 1)then
          read(str(len("BitmapSize=")+1:),*) BitmapSize
      elseif (index(str,"CCD_DETECTOR_DIMENSIONS=") == 1)then
          read(str(len("CCD_DETECTOR_DIMENSIONS=")+1:),*)nx,ny
      elseif (index(str,"CCD_DETECTOR_SIZE=") == 1)then
          read(str(len("CCD_DETECTOR_SIZE=")+1:),*)qx,qy
      endif
   enddo
   close(fd)
   if (HEADER_BYTES*nx*ny*qx*qy .eq. 0) then
      error_flag = 1
      write(0,*)"plugin_get_header: failed reading header of the file"//template(1:lnblnk(template))
      return
   endif
   qx=qx/nx
   qy=qy/ny
   nbyte=4
   number_of_frames=1
   info_array=0
   error_flag=0
END SUBROUTINE plugin_get_header

SUBROUTINE plugin_get_data(frame_number, nx, ny, data_array, info_array, error_flag)  BIND(C,NAME="plugin_get_data")
    USE ISO_C_BINDING
    USE plugin_test_mod
    implicit none
    integer(c_int)                    :: nx, ny, frame_number
    integer(c_int)                    :: error_flag
    integer(c_int), dimension(1024)   :: info_array
    integer(c_int), dimension(nx*ny) :: data_array

    byte head(4096)
    integer i, fd, omp_get_thread_num
    character*1024 fname
    integer*2, dimension(2084*2084) :: shortarray
    !print*,fmt,nx,ny

    fd = 11+omp_get_thread_num()
    fname=template(1:lnblnk(template))
    write(fname(from:to),"("//fmt//")")frame_number
    open(fd,file=fname,form="unformatted",access='direct',recl=HEADER_BYTES+nx*ny*2)
    read(fd,rec=1)head(1:HEADER_BYTES), shortarray(1:nx*ny)
    do i=1,nx*ny 
       if(shortarray(i).eq.0)then
          data_array(i)=-1
       else
          data_array(i)=shortarray(i)
       endif
    enddo
    close(fd)
    error_flag = 1
END SUBROUTINE plugin_get_data

SUBROUTINE plugin_close(error_flag) BIND(C,NAME="plugin_close")
    USE ISO_C_BINDING
    implicit none
    integer(c_int) :: error_flag
    error_flag = 0
END SUBROUTINE plugin_close
